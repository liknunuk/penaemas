<?php error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PADI EMAS</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- bs cdn -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- bs local
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  -->
  <!-- font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px}

    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    #gchat > li.list-group-item > p {margin:0 ; padding: 0;}
    #gchat > li:nth-child(odd){background-color: #DDDDDD; }
    #gchat > li:nth-child(even){background-color: #EEEEEE; }

    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;}
    }
  </style>
  <script src="js/penaemas.js"></script>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>PENA EMAS</h4>
      <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="./?">Utama</a></li>
        <li><a href="./?data=siswa">Siswa</a></li>
        <li><a href="./?data=masalah">Masalah</a></li>
        <li><a href="./?data=gconc">Konseling Group</a></li>
        <li><a href="./?data=concg">Kelompok Konseling</a></li>
      </ul><br>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Blog..">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
      </div>
    </div>

    <div class="col-sm-9">
      <!-- here is the content -->
      <?php include("content.php"); ?>
    </div>
  </div>
</div>

<footer class="container-fluid">
  <p>Pacu Diri dan Evaluasi Menuju Aktifitas Sukses</p>
  <p>
    &copy;2018 sidik wibowo ahmad
  </p>
</footer>

</body>
</html>
