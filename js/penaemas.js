var machine = "http://likgopar.mugeno.org/padiemas/";
function halseb(data,pn){
    let page = pn - 1;
    if( page < 1 ){
        window.location = machine + "?data="+data+"&nh=1";
    }else{
        window.location = machine + "?data="+data+"&nh="+page;
    }
}

function halber(data,pn){
    let page = pn + 1;
    window.location = machine + "?data="+data+"&nh="+page;
}
$(document).ready(function(){

    $("#prb_kelas").change( function(){
        window.location = machine + "?data=masalah&kelas="+$(this).val();
    })

    $('.prb_cont').click( function(){
        let id = $(this).prop('id');
        let sure = confirm('Ubah status menjadi Proses Lanjut ?');
        if( sure == true ){
            $.post( machine + 'statuschg.php' , {
                obj : 'problem',
                oid : id,
                stt : 'Proses Lanjut'
            },function(){
                location.reload();
            })
        }
    })

    $('.prb_stop').click( function(){
        let id = $(this).prop('id');
        let sure = confirm('Ubah status menjadi Tersolusikan ?');
        if( sure == true ){
            $.post( machine + 'statuschg.php' , {
                obj : 'problem',
                oid : id,
                stt : 'Proses Lanjut'
            },function(){
                location.reload();
            })
        }
    })

    $('.msi_stop').click( function(){
        let id = $(this).prop('id');
        let sure = confirm('Ubah status menjadi Selesai ?');
        if( sure == true ){
            $.post( machine + 'statuschg.php' , {
                obj : 'mission',
                oid : id
            },function(){
                location.reload();
            })
        }
    })

    $('.control-nis').keyup( function(){
        let nis = $(this).val();
        let nama = $(this).parent('td').parent('tr').children('td.control-nama');
        if( nis.length == 10 ){
            $.ajax({
                url : 'inquiries.php?mod=getName&nis='+nis ,
                success : function(siswa){
                    nama.text(siswa);
                }
            })
        }
    });

    $(".rgconc").click( function(){
        let tnn = confirm('Group Konseling Akan Dibubarkan ? ');
        if (tnn == true){
            let gid = $(this).prop('id');
            $.post('./?data=agconc&mod=rmov',{gid:gid},function(){
                location.reload();
            })
        }
    })

});