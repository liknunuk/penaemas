<?php 
  require_once("./lib/class.pe.inc.php");
  $pe = new goldenrice();
  if($_GET['mode'] == "baru" ){
    $gid   = "";
    $nis   = "";
    $mod   = "baru";
  }else{
    $gc = $pe->pickone("*","pe_group","groupId",$_GET['gid']);
    $gid   = $gc['groupId'];
    $nis   = $gc['groupMembers'];
    $mod   = "ubah";

    $idx = rtrim(",",$idx);
    $idx = explode(',',$nis);
    $siswa = [];
    for($i = 0 ; $i < 10 ; $i++){
        
        $data = $pe->nis2siswa($idx[$i]);
        array_push($siswa , $data);
    }
    //print_r($siswa);
  }
  
?>
<div>
  <h2>Formulir Kelompok Konseling</h2><br />
</div>
<form action="./?data=agconc&mod=<?=$mod;?>" method="post">
  <div class="form-group row">
      <label for="groupId" class="col-sm-3">Nomor Kelompok</label>
      <div class="col-sm-9">
          <input type="text" name="groupId" id="groupId" class="form-control" value = "<?=$gid;?>" readonly >
      </div>
  </div>
  
  <div class="form-group row">
      <label for="groupMemb" class="col-sm-3">Siswa Anggota</label>
      <div class="col-sm-9">
          <table class="table table-sm">
              <thead>
                  <tr>
                      <th width="170px">Nis</th>
                      <th>Nama Siswa</th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                  
                    for($i = 0 ; $i < 10 ; $i++ ){
                        if($_GET['mode'] == 'baru'){
                            echo "
                            <tr>
                                <td>
                                    <input type='text' class='form-control control-nis' name=groupMemb[] value=''>
                                </td>
                                <td class='control-nama'>
    
                                </td>
                            </tr>
                            ";
                        }else{
                            echo "
                            <tr>
                                <td>
                                    <input type='text' class='form-control control-nis' name=groupMemb[] value='".$siswa[$i]['nis']."'>
                                </td>
                                <td class='control-nama'>
                                    ".$siswa[$i]['namaSiswa']."
                                </td>
                            </tr>
                            ";
                        }

                    }
                  ?>
              </tbody>
          </table>
          
      </div>
  </div>

  <div class="form-group">
    <div style='width:100%; text-align:right; padding-right: 15px;'>
        <button type="submit" class="btn btn-primary" id="gconc-submit">Simpan</button>
    </div>
  </div>
</form>