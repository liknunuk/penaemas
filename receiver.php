<?php
header ("Access-Control-Allow-Origin: *");
//error_reporting(0);
date_default_timezone_set("Asia/Jakarta");
require("lib/class.crud.inc.php");

$pe = new dbcrud();
//print_r($_POST);
if($_POST['dst']){
  $dst = $_POST['dst'];

  if( $dst == 'setProblem'){
    $sets = 'nis,problem_type,problem_item';
    $data = array($_POST['nis'],$_POST['tpe'],$_POST['itm']);
    $pe->insert('pe_problems',$sets,$data);
  }

  if( $dst == "getMyProMises" ){
    $sql = "SELECT problem_id pid,problem_type pty,problem_item pit FROM pe_problems
            WHERE nis = '".$_POST['nis']."' && status ='Berproses'";
    $qry = $pe->transact($sql);

    $proms = array();
    $probs = array();
    while($prb = $qry->fetch()){
      $prob = array('id'=>$prb['pid'],'pty'=>$prb['pty'],'pit'=>$prb['pit']);
      array_push($probs,$prob);

      $sql1 = "SELECT mission_id mid,mission_desc mdc,mission_trgt mtg
                FROM pe_mission WHERE problem_id = '".$prb['pid']."'";
      $qry1 = $pe->transact($sql1);
      $sols = $qry1->rowCount();
      $miss = array();
      if($sols == 0){
        $mis = array('mid'=>0,'mdc'=>'Belum Ada Misi','mtg'=>0);
        array_push($miss,$mis);
        $prom=array('problem'=>$prob,'mission'=>$miss);
      }else{
        /* -- while mission is more than 0 */
        while($prm=$qry1->fetch()){
          $mis = array('mid'=>$prm['mid'],'mdc'=>$prm['mdc'],'mtg'=>$prm['mtg']);
          array_push($miss,$mis);
          $prom=array('problem'=>$prob,'mission'=>$miss);
        }
        /* -- while mission is more than 0 */
      }

      array_push($proms,$prom);
    }
    echo json_encode($proms);

  }

  if($dst == "saveMission"){
    $sets = 'problem_id, mission_desc, mission_trgt';
    $data = array($_POST['pid'],$_POST['mdc'],$_POST['mtg']);
    $pe->insert('pe_mission',$sets,$data);
  }

  if($dst == "saveAction"){
    $sets = 'mission_id,action_desc,action_date';
    $data = array($_POST['mid'],$_POST['adc'],date('Y-m-d'));
    $pe->insert('pe_actions',$sets,$data);
  }

  if( $dst == "saveMyEval" ){
    $nma = $pe->pickone('namaSiswa','pe_siswa','nis',$_POST['nis']);
    $sets = 'action_id,personName,eval_chat';
    $data = array($_POST['aid'],$nma['namaSiswa'],$_POST['evl']);
    $pe->insert('pe_evaluation',$sets,$data);
  }

  if( $dst == "login" ){
    $password = md5($_POST['nis']."**".$_POST['pwd']);
    $cred = $pe->pickone("nis","pe_siswa","passkey",$password);
    echo $cred['nis'];
  }
  
  if( $dst == "getGroup"){
	$qgid = $pe->transact("SELECT * FROM pe_group WHERE `groupMembers` LIKE '%{$_POST['nis']}%' LIMIT 1"); 
	$rgid = $qgid->fetch();
	$gid  = $rgid['groupId'];
	$gmbr = $rgid['groupMembers'];
	$qgid->closeCursor();
	
	$members = rtrim($gmbr , ',');
	$member  = explode(',',$members);
	
	$gdata = [];
	for($i = 0 ; $i < 10 ; $i ++){
		$nama = $pe->pickone('namaSiswa' , 'pe_siswa' , 'nis' , $member[$i]);
		$data = array('nis'=>$member[$i] , 'nama' => $nama['namaSiswa']);
		array_push($gdata,$data);
	}
	
	echo json_encode($gdata, JSON_PRETTY_PRINT);
	
  }
  
  if($dst == 'evalGroup'){
	  $sql = "INSERT INTO pe_groupChat (groupTopic,participant,shout) VALUES ( ? , ? , ? )";
	  $qry = $pe->transact($sql, array($_POST['pid'] , $_POST['nis'] , $_POST['eva']));
	  $qry->closeCursor();
  }
}

if($_GET){
  $obj = $_GET['obj'];
  if( $obj == 'myprobs'){
    $myProbs = $pe->picksome("*","pe_problems","nis='".$_GET['nis']."' && status='Berproses'");
    echo json_encode($myProbs);
  }

  if($obj == "aksiku"){
    $sql = "SELECT pe_problems.nis,pe_actions.*, mission_desc, pe_mission.status
            FROM pe_problems,pe_actions, pe_mission
            WHERE pe_actions.mission_id = pe_mission.mission_id &&
                  pe_mission.problem_id = pe_problems.problem_id &&
                  pe_problems.nis = '".$_GET['uid']."' &&
                  pe_mission.status ='Tertunda'
            ORDER BY pe_actions.action_date DESC";
    $qry = $pe->transact($sql);
    $acts = array();
    while($r = $qry->fetch()){
      array_push($acts,$r);
    }
    echo json_encode($acts);
  }

  if($obj == "getaksi"){
    $aksi = $pe->pickone("problem_type pcat,problem_item pdsc ,mission_desc mdsc,
                          mission_trgt mtgt,action_date adte, action_desc adsc",
                          "viewActions","action_id",$_GET['aid']);
    echo json_encode($aksi);
  }

  if($obj == "sec" ){
    $chats=$pe->picksome("logTime,personName,eval_chat","pe_evaluation","action_id='".$_GET['aid']."'");
    $i = 0;
    while( $i < COUNT($chats)){
      echo "
      <li class='list-group-item'>
        <div class='personTime'>".$chats[$i]['personName']." on ".$chats[$i]['logTime']."</div>
        <div class='shout-text'>".$chats[$i]['eval_chat']."</div>
      </li>
      ";
      $i++;
    }
  }
  
  if($obj == 'dukawan'){
	  $nis = $_GET['nis'];
	  $sql = "SELECT`problem_id` `id` , `problem_item` `item` FROM `pe_problems` WHERE `nis`='{$nis}' && `status` = 'Berproses'"; 
	  $qry = $pe->transact($sql);
	  $masalah = [];
	  while($res = $qry->fetch()){
		  array_push($masalah , $res);
	  }
	  
	  echo json_encode($masalah , JSON_PRETTY_PRINT);
	  $qry->closeCursor();
  }
  
  if($obj == 'probitem'){
	  $problem = $pe->pickone('problem_item item','pe_problems','problem_id',$_GET['pid']);
	  echo $problem['item'];
  }
  
  if($obj == 'gchat' ){
	  $sql = "SELECT `chatTime` AS `waktu`, `pe_siswa`.`namaSiswa` AS `siswa`, `shout` FROM `pe_groupChat` , `pe_siswa` WHERE `groupTopic`= ? && `pe_siswa`.`nis` = `pe_groupChat`.`participant`";
	  $qry = $pe->transact($sql , array($_GET['pid']));
	  $chats = [];
	  while($res = $qry->fetch()){
	    array_push($chats , $res);   
	  }
	  echo json_encode($chats);
	  $qry->closeCursor();
  }
}

?>
