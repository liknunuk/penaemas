-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 24 Jun 2019 pada 12.50
-- Versi server: 5.6.44-log
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klubaner_padiemas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pe_actions`
--

DROP TABLE IF EXISTS `pe_actions`;
CREATE TABLE `pe_actions` (
  `action_id` int(6) UNSIGNED NOT NULL,
  `mission_id` int(5) UNSIGNED NOT NULL,
  `action_date` date DEFAULT NULL,
  `action_desc` tinytext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pe_actions`
--

INSERT INTO `pe_actions` (`action_id`, `mission_id`, `action_date`, `action_desc`) VALUES
(3, 4, '2019-06-24', 'Sikat gigi pagi pakai sirih'),
(4, 8, '2019-06-24', 'Setelah salat subuh membuka buku lalu mengulas materi'),
(5, 10, '2019-06-24', 'Setelah sholat subuh, tadarus alquran minimal 5 menit, lalu buka buku pepajaran'),
(6, 7, '2019-06-24', 'Belajar setiap hari'),
(7, 7, '2019-06-24', 'Menambah jam belajar'),
(8, 16, '2019-06-24', 'Posting kemampuan sulap'),
(9, 17, '2019-06-24', 'Nonton film durasi 30 menit'),
(10, 18, '2019-06-24', 'Belajar dengan durasi yang lebih');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pe_evaluation`
--

DROP TABLE IF EXISTS `pe_evaluation`;
CREATE TABLE `pe_evaluation` (
  `eval_id` int(8) UNSIGNED NOT NULL,
  `action_id` int(6) UNSIGNED NOT NULL,
  `logTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `personName` varchar(30) DEFAULT NULL,
  `eval_chat` tinytext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pe_evaluation`
--

INSERT INTO `pe_evaluation` (`eval_id`, `action_id`, `logTime`, `personName`, `eval_chat`) VALUES
(1, 1, '2019-06-23 02:17:49', 'Muhammad Risyad Ma`Ruf', 'Masih ngantukan'),
(2, 2, '2019-06-24 02:18:23', 'Arif Ramadan', 'Masih ompong'),
(3, 5, '2019-06-24 02:20:37', 'Guru BK', 'Masih ngantukan?'),
(4, 4, '2019-06-24 02:21:48', 'Guru BK', 'Bagaana hasilnya?'),
(5, 13, '2019-06-24 02:57:28', 'Elinda Yunitasari', 'Tenyata yang ngekek malah follower'),
(6, 3, '2019-06-24 03:03:45', 'Guru BK', 'Sehari berapa mapel?'),
(7, 1, '2019-06-24 05:44:29', 'Guru BK', 'Coba ganti gaya push upnya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pe_group`
--

DROP TABLE IF EXISTS `pe_group`;
CREATE TABLE `pe_group` (
  `groupId` int(4) NOT NULL,
  `groupMembers` tinytext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pe_groupChat`
--

DROP TABLE IF EXISTS `pe_groupChat`;
CREATE TABLE `pe_groupChat` (
  `chatTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `groupTopic` int(4) NOT NULL,
  `participant` varchar(10) DEFAULT NULL,
  `shout` tinytext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pe_groupTopics`
--

DROP TABLE IF EXISTS `pe_groupTopics`;
CREATE TABLE `pe_groupTopics` (
  `gtId` int(4) NOT NULL,
  `groupId` int(4) NOT NULL,
  `initiator` varchar(10) NOT NULL,
  `problem_type` varchar(11) DEFAULT 'Kepribadian',
  `topic` tinytext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pe_mission`
--

DROP TABLE IF EXISTS `pe_mission`;
CREATE TABLE `pe_mission` (
  `mission_id` int(5) UNSIGNED NOT NULL,
  `problem_id` int(4) UNSIGNED NOT NULL,
  `mission_sq` int(2) UNSIGNED DEFAULT '1',
  `mission_desc` varchar(50) DEFAULT NULL,
  `mission_trgt` varchar(50) DEFAULT NULL,
  `logTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Selesai','Tertunda') DEFAULT 'Tertunda'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pe_mission`
--

INSERT INTO `pe_mission` (`mission_id`, `problem_id`, `mission_sq`, `mission_desc`, `mission_trgt`, `logTime`, `status`) VALUES
(1, 1, 1, 'Push Up setiap ganti pelajaran', '5x', '2019-06-23 02:15:50', 'Tertunda'),
(2, 18, 1, 'Selalu olahraga ', 'Olahraga terus ', '2019-06-24 02:12:03', 'Tertunda'),
(3, 18, 1, 'Selalu olahraga ', 'Olahraga terus ', '2019-06-24 02:12:05', 'Tertunda'),
(4, 8, 1, 'Rutin sikat gigi', '3 x sebari', '2019-06-24 02:13:16', 'Tertunda'),
(5, 12, 1, '', '', '2019-06-24 02:13:17', 'Tertunda'),
(6, 12, 1, '', '', '2019-06-24 02:13:17', 'Tertunda'),
(7, 3, 1, 'Menstabilkan nilai rapot untuk bertahan dalam stan', 'Bisa meraih nilai rapot yg stabil', '2019-06-24 02:14:20', 'Tertunda'),
(8, 13, 1, 'Belajar lebih rajin lagi', 'Belajar sebelum berangkat ke sekolah', '2019-06-24 02:14:38', 'Tertunda'),
(9, 14, 1, 'Rutin belajar', '2 kali sehari', '2019-06-24 02:14:55', 'Tertunda'),
(10, 9, 1, 'Wudhu sebelum belajar', 'Belajar sehabis sholat subuh', '2019-06-24 02:15:55', 'Tertunda'),
(11, 17, 1, 'Rutin belajar di waktu luang walau tidak ada ulang', '6 hari dalam seminggu', '2019-06-24 02:17:05', 'Tertunda'),
(12, 30, 1, 'Saya tetap mendaftar kedinasan Akpol dan juha kuli', 'Saya akan berlatih fisik, dan belajar secara maksi', '2019-06-24 02:17:20', 'Tertunda'),
(13, 22, 1, 'Olahraga ', 'Tinggi badan min 167', '2019-06-24 02:18:30', 'Tertunda'),
(14, 42, 1, 'Bangun lebih pagi', 'Bangun jam 04:30 setiap hari', '2019-06-24 02:19:14', 'Tertunda'),
(15, 45, 1, 'Mulai meminjam buku perpustakaan', 'Meminjam rutin seminggu rutin', '2019-06-24 02:53:29', 'Tertunda'),
(16, 47, 1, 'Punya follower IG', '1000 fillower', '2019-06-24 02:54:45', 'Tertunda'),
(17, 48, 1, 'Mengurangi durasi nonton film', 'Menonton film maksimal 1 jam', '2019-06-24 02:58:23', 'Tertunda'),
(18, 49, 1, 'Menambah jam belajar', 'Belajar dengan duradi yang lebih lama', '2019-06-24 03:08:36', 'Tertunda'),
(19, 50, 1, 'Rutin membaca Al-Qur\'an dan melaksanakan shalat su', 'Dilakukan setiap hari secara rutin', '2019-06-24 03:10:23', 'Tertunda');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pe_problems`
--

DROP TABLE IF EXISTS `pe_problems`;
CREATE TABLE `pe_problems` (
  `problem_id` int(4) UNSIGNED NOT NULL,
  `nis` int(5) DEFAULT NULL,
  `problem_type` varchar(11) DEFAULT 'Kepribadian',
  `problem_item` tinytext,
  `logTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Berproses','Proses Lanjut','Tersolusikan') DEFAULT 'Berproses'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pe_problems`
--

INSERT INTO `pe_problems` (`problem_id`, `nis`, `problem_type`, `problem_item`, `logTime`, `status`) VALUES
(3, '0024271673', 'Karir', 'Bingung meneruskan ke PTN mana', '2019-06-24 02:08:20', 'Berproses'),
(4, '0017194464', 'Belajar', 'Kurang semangat dalam belajar', '2019-06-24 02:08:24', 'Berproses'),
(5, '0025426964', 'Belajar', 'Masih malas belajar', '2019-06-24 02:08:31', 'Berproses'),
(6, '0028649773', 'Belajar', '', '2019-06-24 02:08:59', 'Berproses'),
(7, '2147483647', 'Sosial', 'Saya orangnya susah bergaul, tapi saya mau ngerubahnya', '2019-06-24 02:09:06', 'Berproses'),
(8, '0027364244', 'Karir', 'Ingin jadi pilot tapi ompong', '2019-06-24 02:09:13', 'Berproses'),
(9, '0020701793', 'Belajar', 'Jika belajar langsung ngantuk', '2019-06-24 02:09:16', 'Berproses'),
(10,'0027364244', 'Karir', 'Ingin jadi pilot tapi ompong', '2019-06-24 02:09:18', 'Berproses'),
(11, '2147483647', 'Sosial', 'Saya susah bergaul, gimana merubahnya', '2019-06-24 02:09:50', 'Berproses'),
(12, '0027125524', 'Karir', 'Masih bingung untuk menentukan jurusan kuliah, karena saya anak ipa tapi lebih tertarik ke jurusan ips saat kuliah', '2019-06-24 02:09:50', 'Berproses'),
(13, '0011898564', 'Belajar', 'Masih malas belajar, belum ada motivasi dari dalam diri ubtuk semangat belajar', '2019-06-24 02:09:54', 'Berproses'),
(14, '0024093980', 'Belajar', 'Susah belajar', '2019-06-24 02:10:00', 'Berproses'),
(15, '0017072637', 'Karir', 'belum bisa memahami passion diri sendiri, ingin sekali dan sudah tertarik kuliah di jurusan psikologi tapi kata kakak kelas bahwa jurusan psikologi memiliki peluang kerja yang sedikit. saya harus bagaimana?', '2019-06-24 02:10:00', 'Berproses'),
(16, '2147483647', 'Belajar', 'Fisika susah', '2019-06-24 02:10:12', 'Berproses'),
(17, '0011567803', 'Belajar', 'Malas belajar, kalau tidak ada ulangan atau tugas', '2019-06-24 02:10:16', 'Berproses'),
(18, '0030316141', 'Karir', 'Saya ingin menjadi seorang tentara, saya ingin menempuh pendidikan di akademi angkatan udara tetapi umur saya belum pas saat saya lulus dari man 2, saya harus menunggu 1 tahun terlebih dahulu, fisik saya juga masih kurang, tinggi badan saya masih kurang, ', '2019-06-24 02:10:28', 'Berproses'),
(19, '2147483647', 'Kepribadian', 'Saya berkepribadian ganda', '2019-06-24 02:10:36', 'Berproses'),
(20, '0011567803', 'Belajar', 'Malas belajar, kalau tidak ada ulangan atau tugas', '2019-06-24 02:10:58', 'Berproses'),
(21, '0027364244', 'Karir', 'Ingin jadi ABRI tapi pendek', '2019-06-24 02:11:00', 'Berproses'),
(22, '0030316141', 'Karir', 'Saya ingin menjadi seorang tentara, saya ingin menempuh pendidikan di akademi angkatan udara tetapi umur saya belum pas saat saya lulus dari man 2, saya harus menunggu 1 tahun terlebih dahulu, fisik saya juga masih kurang, tinggi badan saya masih kurang, ', '2019-06-24 02:11:01', 'Berproses'),
(23, '0024093980', 'Belajar', 'Susah belajar', '2019-06-24 02:11:15', 'Berproses'),
(24, '2147483647', 'Sosial', 'Saya susah bergaul, saya mau merubahnya', '2019-06-24 02:11:26', 'Berproses'),
(25, '0025426964', 'Karir', '', '2019-06-24 02:11:33', 'Berproses'),
(26, '0024115646', 'Apa Jenis M', '', '2019-06-24 02:11:45', 'Berproses'),
(27, '2147483647', 'Belajar', 'Susah belajar fisika', '2019-06-24 02:12:07', 'Berproses'),
(28, '2147483647', 'Belajar', 'Saya susah belajar fisika', '2019-06-24 02:12:41', 'Berproses'),
(29, '0024075959', 'Belajar', 'Sulit berkonsentrasi saat belajar', '2019-06-24 02:12:44', 'Berproses'),
(30, '0021292618', 'Karir', 'Saya bingung ingin meneruskan kedinasan Akpol, tapi saya masih banyak kekurangannya. Sedangkan disisi lain saya ingin kuliah umum, tapi kalau saya ingin kuliah umum saya bingung dengan jurusan apa ? Dan saya mempunyai cita - cita menjadi polwan ', '2019-06-24 02:13:09', 'Berproses'),
(31, '0024075959', 'Belajar', 'Sulit berkonsentrasi saat belajar', '2019-06-24 02:13:35', 'Berproses'),
(32, '0024075959', 'Belajar', 'Sulit berkonsentrasi saat belajar', '2019-06-24 02:13:35', 'Berproses'),
(33, '2147483647', 'Belajar', 'Saya susah belajar fisika', '2019-06-24 02:13:54', 'Berproses'),
(34, '0024075959', 'Belajar', 'Sulit berkonsentrasi saat belajar', '2019-06-24 02:14:21', 'Berproses'),
(35, '0027125524', 'Karir', 'Masih bingung untuk menentukan jurusan kuliah, karena saya anak ipa tapi lebih tertarik ke jurusan ips saat kuliah', '2019-06-24 02:14:49', 'Berproses'),
(36, '0025426964', 'Apa Jenis M', '', '2019-06-24 02:15:32', 'Berproses'),
(37, '0028649773', 'Belajar', 'Belum bisa belajar setiap hari dg rajin', '2019-06-24 02:17:42', 'Berproses'),
(38, '0030316141', 'Karir', 'Saya ingin menjadi seorang tentara, saya ingin menempuh pendidikan di akademi angkatan udara tetapi umur saya belum pas saat saya lulus dari man 2, saya harus menunggu 1 tahun terlebih dahulu, fisik saya juga masih kurang, tinggi badan saya masih kuran', '2019-06-24 02:17:51', 'Berproses'),
(39, '0024115646', 'Belajar', 'Belum bisa belajar dengan rajin', '2019-06-24 02:17:54', 'Berproses'),
(40, '0030316141', 'Karir', 'Saya ingin menjadi seorang tentara, saya ingin menempuh pendidikan di akademi angkatan udara tetapi umur saya belum pas saat saya lulus dari man 2, saya harus menunggu 1 tahun terlebih dahulu, fisik saya juga masih kurang, tinggi badan saya masih kuran', '2019-06-24 02:17:55', 'Berproses'),
(41, '0024271673', 'Karir', 'Bingung meneruskan ke PTN mana', '2019-06-24 02:17:56', 'Berproses'),
(42, '0027125524', 'Kepribadian', 'Saya memiliki sifat pemalas, bagaimana cara menghilangkan sifat pemalas?', '2019-06-24 02:18:16', 'Berproses'),
(43, '0021292618', 'Apa Jenis M', '', '2019-06-24 02:18:26', 'Berproses'),
(44, '0021936324', 'Belajar', 'Saya membutuhkan dorongan dan buku materisl', '2019-06-24 02:51:26', 'Berproses'),
(45, '0021936324', 'Belajar', 'Saya membutuhkan dorongan dan buku materi', '2019-06-24 02:51:41', 'Berproses'),
(46, '0021936324', 'Belajar', 'Saya butuh kuota', '2019-06-24 02:52:12', 'Berproses'),
(47, '0007056053', 'Sosial', 'Sering diejek cebol', '2019-06-24 02:53:29', 'Berproses'),
(48, '0024271673', 'Kepribadian', 'Terkadang menonton film dengan durasi yang berlebih', '2019-06-24 02:54:17', 'Berproses'),
(49, '0024271673', 'Belajar', 'Kurang durasi jam belajar', '2019-06-24 03:06:58', 'Berproses'),
(50, '0023970510', 'Kepribadian', 'Kurang dalam hal beribadah', '2019-06-24 03:07:39', 'Berproses');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pe_siswa`
--

DROP TABLE IF EXISTS `pe_siswa`;
CREATE TABLE `pe_siswa` (
  `nis` varchar(10) NOT NULL,
  `namaSiswa` varchar(40) DEFAULT NULL,
  `kelas` varchar(10) DEFAULT NULL,
  `passkey` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pe_siswa`
--

INSERT INTO `pe_siswa` (`nis`, `namaSiswa`, `kelas`, `passkey`) VALUES
('0023475299', 'Akhida Fahriani', 'XI.IPA.1', '01f0835717282f5a05e7e815d247de6e'),
('0011931693', 'Aldinova Gondo Ruming Asmoro', 'XI.IPA.1', '71534c60e8da219691d7e2de2af904b0'),
('0017194458', 'Aldo Aditia Lembana', 'XI.IPA.1', '34289b97ed6fff5c74c142229ea10991'),
('0024271637', 'Anisa Kurnia Sabarina', 'XI.IPA.1', '79df80052905589a09c47af1fde98272'),
('0023146867', 'Arif Subekti', 'XI.IPA.1', '18a614fea8d6d6cd23e1e9305d50ba4e'),
('0023617117', 'Arrazzaq Panca Putra', 'XI.IPA.1', '498ba6605049f731c71bb0dfd3c0ab1f'),
('0035349643', 'Atina Fadhilah', 'XI.IPA.1', 'cc11fb2a7b55e4d71d84ff486c7448c9'),
('0014204305', 'Atta Nasyitul Khoiriyah', 'XI.IPA.1', 'e245840a85c44e0bd9f0ad6f549f66e6'),
('0024239450', 'Ayu Dwi Yuliana Firdaus', 'XI.IPA.1', 'd00b51f786612398bd6ffe53b7b60f0a'),
('0014343286', 'Carin Nova Azzaria', 'XI.IPA.1', 'dd97573762c0399dae4e6a1eacd73c7a'),
('0024086546', 'Dwi Ike Rahmawati', 'XI.IPA.1', '1460022b12f6f0a402926cc552619fd0'),
('0012281230', 'Dwi Ramadha Hening Utami', 'XI.IPA.1', '4ccbabc69d2cc6df117c484e6b54f435'),
('0023617086', 'Dzulfa Anisa Cahyadewi', 'XI.IPA.1', '0490c6e41db7892b8ceca940ee3eaaf8'),
('0022501110', 'Fatma Aftiana', 'XI.IPA.1', '8378477ff031e774df3e947d07be56d4'),
('0011864239', 'Fitria Nur Aeni', 'XI.IPA.1', 'fae2dafa3e8154d10b9a2d1c017780cc'),
('0023617070', 'Gholin Arbi Fanani', 'XI.IPA.1', '0e1de789eb5f4354ddca314a081603d0'),
('0021394446', 'Hanif Nur Alaili', 'XI.IPA.1', 'e5010fd4e56b9d6b79bcf1e9b6fa73eb'),
('0026558403', 'Hesti Kurniasih', 'XI.IPA.1', '5271ffa48d88bccf0bccc79c117bf31e'),
('0036561843', 'Hilmi Alfaroh', 'XI.IPA.1', 'c340d86e887fae5216daad018976b1fa'),
('0017489345', 'Ilham Yoga Pratama', 'XI.IPA.1', 'dd128ea4fc48e68aaeb45b90b0efa2a7'),
('0024250421', 'Inayah Wulan Septiani', 'XI.IPA.1', '36bca0ec01c77b28d54d722fd863409f'),
('0025220240', 'Isti Puji Rahayu', 'XI.IPA.1', '8ca2f15aaf03bf0492ab87e33118270b'),
('0017693436', 'Jaenal Tri Yusuf', 'XI.IPA.1', 'bf4bcf1e65f8be43dd7db2bfd35a74ad'),
('0039713225', 'Khustiana Adinda Zahra', 'XI.IPA.1', '33cd05d847fc194bc1d609996619add7'),
('0023872899', 'Latifah Ayu Mujahidah', 'XI.IPA.1', 'd71adc653632bf3cfc2ca1f5d5ea3d9d'),
('0025745757', 'Lindiawati', 'XI.IPA.1', 'bad5e24c34003c60f6cbcf3d211ccf44'),
('0028808831', 'Lutfiyah Miftahul Jannah', 'XI.IPA.1', '9565bc9aa00784ae5d79a53cc6c61dd2'),
('0017050100', 'Muhammad Risyad Ma`Ruf', 'XI.IPA.1', '479e4185c5dfeae4c9dba2b64f9b433f'),
('0017693437', 'Muhamad Haerul Erlan Hidayatulloh', 'XI.IPA.1', '552108852d56a870dfb1c00528a48a6f'),
('0029435310', 'Muhammad Feri Tajudin', 'XI.IPA.1', '87d9f7b74137c598a4983d4e07ef3797'),
('0028502752', 'Nabilah Muliawati Kusuma Wildan', 'XI.IPA.1', '6f0c4d5a1a7a3897152576cd6c6a8505'),
('0023475125', 'Ngadhilah Aningtias', 'XI.IPA.1', '8e58c67fc84a366cd1f78d84844f1680'),
('0028190831', 'Nur Baeti Lega Wahidah', 'XI.IPA.1', 'acab3cb74c8f5b50c39279ea3cf80b18'),
('0028011848', 'Nurul Huda Ngainul Yakin', 'XI.IPA.1', 'e5546ce26d3bbe86d7c9aec193f69e6c'),
('0017337227', 'Raja Nuraina Fatwah', 'XI.IPA.1', 'f5604d98318b8a98c9b86f2a6f809627'),
('0019284800', 'Rashika Ardafa Sahila', 'XI.IPA.1', '57d123fad15d54c19298e8933daf83df'),
('0024859699', 'Riski Amelia', 'XI.IPA.1', '8928320985f5409726201470a54267f0'),
('0028928079', 'Rizky Nur Hidayat', 'XI.IPA.1', '15502edf8e384a365a76c93a98d4ab69'),
('0028187885', 'Sakinah Rahmawati', 'XI.IPA.1', 'cc1b87d3504af32012051780c4c09d60'),
('0023475277', 'Shinta Ayu Maharani', 'XI.IPA.1', '3aa2d652d70674a87e9d3a2eb6123596'),
('0023475133', 'Tabah Nugroho', 'XI.IPA.1', 'bdbba00599fb94bf427eaf5687499b3f'),
('0023872912', 'Tri Antini', 'XI.IPA.1', '5781fa7c4b8ce97700ff7efe1d4f6f9c'),
('0015602311', 'Tsania Citra Madani', 'XI.IPA.1', 'b08398d2a3e4c1fe2f97d71295aae021'),
('0017853311', 'Vina Amalia Rahmah', 'XI.IPA.1', '87ee5ce52aab401d06ed17275ed3a68c'),
('0005696445', 'Aldi Hidayat', 'XI.IPA.2', 'b10d95facfc23cabe2d51fdb3997a849'),
('0029043820', 'Aldiyan Ahmad Febriyanto', 'XI.IPA.2', 'fb3648a1512f2d912245effaf75fbb92'),
('0026424587', 'Alif Hidayah', 'XI.IPA.2', '456c442a3770b9b0b4cd5924fd65f723'),
('0027125524', 'Annisa Al Inayah', 'XI.IPA.2', 'd6c65ad8196bb8001f0c5c5fbc109c90'),
('0011567803', 'Atika Sofiana', 'XI.IPA.2', 'b5ff7f9e0bf1f7119763b297c47917a6'),
('0020701793', 'Ayu Faradita', 'XI.IPA.2', 'a367d90ec425ac8e328638ccd0795e76'),
('0024239264', 'Dea Nanda Amelia', 'XI.IPA.2', '6958622b35c564c28eaf6c7fc231b075'),
('0024115448', 'Dimas Syahrul Alam Syah', 'XI.IPA.2', '384b02429bd1e0430e1b8105be4fb829'),
('0024075959', 'Eko Prabowo', 'XI.IPA.2', 'f7460db7a0dc0807cf2da4dc4f1710d3'),
('0021292618', 'Elista Fitriyatun Setyaningsih', 'XI.IPA.2', '5371efb2ade6141397acfb4b1f30d4a4'),
('0030316141', 'Essa Cahyaning Agusti', 'XI.IPA.2', '7406bec014f990992290d1e7ca868a8a'),
('0023475134', 'Femilia Aula Fidiah', 'XI.IPA.2', 'd7116b18a57d523c956d0c4c5b315d95'),
('0023970510', 'Fika Fathu Rohmah', 'XI.IPA.2', '8feecb8e9d2f10a7d9f531ae4a637820'),
('0021936324', 'Hanif Umar Said', 'XI.IPA.2', 'd7a8bf37929a3d6f91b72aa36503d3b7'),
('0017318084', 'Hanin Dwi Ramdhani', 'XI.IPA.2', 'feb04f169e0907c2fe82064dc018bcab'),
('0017121503', 'Ikmal Prasetyo', 'XI.IPA.2', '8d75b72c9243a7bc1986d478aa10e9b4'),
('0023735272', 'Ilma Dhiyaulhayi Al-Lust', 'XI.IPA.2', 'b0ecb5470c13b143843539e592b1e3d5'),
('0024271673', 'Isnanda Danur Nugroho', 'XI.IPA.2', '458a50de4f41c7b814eaa209fe5f9951'),
('0022920663', 'Khoeriyah', 'XI.IPA.2', 'd83c8731c3d078e1b988888826c1848b'),
('0017072637', 'Khorina Rahmadhani', 'XI.IPA.2', '5df5d26f0102f2bc01a53d5d9547abae'),
('0023858109', 'Khusna Setiowati', 'XI.IPA.2', '25280ba68ae4a1c4b75cc2c8cb7295e8'),
('0020740624', 'Lenisa Mahar Dinta ', 'XI.IPA.2', '29ec8d4f6175fa972d362918ed06205b'),
('0017194467', 'Lintang Hafidh Faruq Aqilah', 'XI.IPA.2', '89a6eb35cf78f691e0c553fdd0714f27'),
('0025426964', 'Lu`Lu Lutfiyani', 'XI.IPA.2', '8363cf10454c2de7bf2ec3bace1a8cba'),
('0024115646', 'Lyan Rahma Syarifah', 'XI.IPA.2', '8f16b97aca660d6cabb2b53f1eb999c4'),
('0017282292', 'Maulina Nofita Sari', 'XI.IPA.2', '5cc6881cc6797be9c25ba5b0fa043897'),
('0029322807', 'Muhammad Faris Mufid', 'XI.IPA.2', '940c38663d9cad9d6df647a25c36642e'),
('0023451135', 'Muhammad Ghufron Mubarak', 'XI.IPA.2', '2da6d3110686d6861e208e016076d682'),
('0028649773', 'Nanda Agesti', 'XI.IPA.2', '571852060d5db2ae0ff2888bd5d1237f'),
('0023971668', 'Ngazzah Hanun Nabilah', 'XI.IPA.2', '12ee79ca47dab75e4f39f3f0c6bef52f'),
('0024093980', 'Oktaviana Dwi Nata', 'XI.IPA.2', '9cd3a39efce1d85f11319b7c155fc0a2'),
('0027795000', 'Rafi Bagus Aditya', 'XI.IPA.2', 'c003feba23ec9532b4320bb7ebce8f42'),
('0017337105', 'Ridha Idham Ismaini', 'XI.IPA.2', '0059333b403ed8f6573636ee13b94c1d'),
('0027943253', 'Rizqi Ardika Akbar', 'XI.IPA.2', '0e14693226dd72d661c8c2df53064bcf'),
('0017194464', 'Safilla Azhari Budiono', 'XI.IPA.2', 'a04a05bceb3ef7ebb38f5de5c59359e5'),
('0017400355', 'Salma Aulika Haryanto', 'XI.IPA.2', 'c96d18febd79271b6b70a38917f804e2'),
('0029247205', 'Siti Noviana', 'XI.IPA.2', '0a46fccee22bf3a51f16de483ef5e430'),
('0023451474', 'Syifa Yunita Fadila', 'XI.IPA.2', '79511c4756337d00d4fa4be47c0447cf'),
('0022103429', 'Widy Rakhayu', 'XI.IPA.2', '7ad96b33263a2ecac10b347c4c54b8e5'),
('0028099464', 'Wildan Masruriana', 'XI.IPA.2', '5499a75b6699faa5b4b21f638aa361ef'),
('0024271918', 'Yuda Prasetyo', 'XI.IPA.2', '0ea7b98af33dc519624d6039398b817d'),
('0011898564', 'Yulia Rahmawati', 'XI.IPA.2', 'bb4da37fd3fb064061e8f43543a5c2c6'),
('9015739206', 'Muhammad Rizky Zulfa Zain', 'XI.IPA.2', '4d2ebc24aa1413d3533ac3717f896608'),
('0017039793', 'Adinda Sekar Noviana', 'XI.IPA.3', '26c47275f6aed3de6045d0989ad5783b'),
('0028099456', 'Aldila Faqih Surya', 'XI.IPA.3', '5876a24c82df591ba45d7a2a671b5822'),
('0024271671', 'Alfaridzi Nur Muadzin', 'XI.IPA.3', '5bf20cba32c4d3fa055a9de926def5cb'),
('0027972497', 'Ariangga Achmad', 'XI.IPA.3', '4129e766cb38f1c5fe522d6f08197c37'),
('0023937122', 'Arum Cahyaningtiyas', 'XI.IPA.3', '50639ea893ae9269c0916e75904ad8c5'),
('0016478575', 'Ayu Annisa', 'XI.IPA.3', '3f3d7b054816f6e4b1e4dbd6b5ea0d06'),
('0023513896', 'Az Zahra Anugraini Anwar', 'XI.IPA.3', '78e9efb0844a0bf0c4e4fa0bc7476a65'),
('0017554832', 'Desi Fitriyani', 'XI.IPA.3', '4eaadcc5420fdef782e44f707866ffd9'),
('0020740718', 'Diesta Nurfitria Oktafiani', 'XI.IPA.3', '96a89604261a00e622549813222581fe'),
('0017830500', 'Dimas Rizqianto Jati Prabowo', 'XI.IPA.3', '57dddbb1041b2bdd9a221cd5897abe0b'),
('0024001888', 'Dito Nur Arifin', 'XI.IPA.3', 'd4bba31b115bcb2177ae28be5809b1c8'),
('0020099613', 'Elsa Febriyani', 'XI.IPA.3', '87e14656c7d83c5ec6de8e7d9a2ce99c'),
('0020011653', 'Enggar Akbar Nur Izza', 'XI.IPA.3', '646431f35bf993feb2bcd2b527a02aca'),
('0025622735', 'Fadhilah Azzafira Labibah', 'XI.IPA.3', 'd6ddd3b6066696d43ba0c5b1f804cd70'),
('0023475190', 'Fidianti', 'XI.IPA.3', 'f233cbfb3e976ad846ee79f3ec260522'),
('0013796164', 'Fitriyani Nurjanah', 'XI.IPA.3', '6e61576ec10e8cf3dd5be610ec71e3b8'),
('0019492850', 'Hanni` Ma`Rifatul Amanah', 'XI.IPA.3', 'a9d309922c91bbaf734b478056442b32'),
('0023916431', 'Hilman Navi`An', 'XI.IPA.3', '32ea86ee2f868bc2543370a9481ca5b3'),
('0021788519', 'Imtikhan Maulid', 'XI.IPA.3', '89c20798a1dda51efeed8621f0579641'),
('0017830460', 'Khikmah Dela Nadiati', 'XI.IPA.3', '56a0a3c28263bbbc1d1c485b63861e86'),
('0017853305', 'Lailia Nur Baity', 'XI.IPA.3', '5066c42697a75da97ae2c85544217506'),
('0029216386', 'Lia Triana', 'XI.IPA.3', 'f6c39888789e250a5770fae2044b3b81'),
('0017318047', 'Muhamad Adrian Reza Reviansyah', 'XI.IPA.3', '1cc1b734805f1388c96e25625d8661c1'),
('0012339654', 'Muhammad Hikam Fikri Mubarok', 'XI.IPA.3', '6c32e9604e7059a98bc6fefce00e9d01'),
('0029691972', 'Maulati  Setianingrum', 'XI.IPA.3', '27443e49227990ff66fa1d85c8e838ba'),
('0011898558', 'Meistri Muldhi Astuti', 'XI.IPA.3', '41657162872f0381287438bc84598449'),
('0029664447', 'Muammar Yasir Husain', 'XI.IPA.3', '33234b70a08117fc1feff94bd4e7e444'),
('0020342833', 'Muhammad Al Fikri Saifudin', 'XI.IPA.3', 'e8ca76880f3f48f367b025bb9b985341'),
('0023970830', 'Novia Rachmawati', 'XI.IPA.3', '7921877dcdc52d4271d1a8dcff2d89d5'),
('0017515684', 'Pratiwi Mustikawati', 'XI.IPA.3', '5990d884f557a4e636e4c2cc596822e9'),
('0023735320', 'Ridho Nur Barokah', 'XI.IPA.3', '9a30712ab960a43b0280cd2be244ce4a'),
('0024115868', 'Rifki Figianto', 'XI.IPA.3', 'e0c11c41b761b96f608cfb50818e0cea'),
('0022885383', 'Riska Amalia', 'XI.IPA.3', '63a0ed34ed0df152282028d8530829bf'),
('0023435591', 'Safira Ngesti Pamuji', 'XI.IPA.3', '0de9c80c3ca28e6cfaf68f29f590751d'),
('0002585271', 'Sasti Murti Devi', 'XI.IPA.3', '284036a56960d8dd824637f52c34793a'),
('0020669974', 'Satrio Setia Wiguno', 'XI.IPA.3', 'd1e1e2736c8362dce4c497697142b7fd'),
('0017470511', 'Suchi Mafiroh', 'XI.IPA.3', '879338f53d9e3faf6a8eec8aa2a24efb'),
('0020669868', 'Syarifatun Nur Fadlilah', 'XI.IPA.3', '2c78f0a0027d2b9472da2e51da71d1f4'),
('0029586925', 'Vika Oktaviani', 'XI.IPA.3', '60041f3ee37b52c1b2612fcbfaafbb62'),
('0023347054', 'Wiwit Widya Lestari', 'XI.IPA.3', '55f4d9e70d9c4b36d75a33ebc5f96065'),
('0017098328', 'Yafika Aribah', 'XI.IPA.3', '07b89c6839443f14dc18de8208bef96b'),
('0027972493', 'Yonanda Ikhwan Adib', 'XI.IPA.3', '84088cd90129c15894cacbbd79616fb8'),
('0027972520', 'Rafid Maulana Fadlurrahman', 'XI.IPA.3', '8546e06d9ef030ce2532da1a53e45aec'),
('0032787653', 'Aldi Fiyanto', 'XI.IPA.OK', '8e9cc1c820baa8c2ae965feddbb6675e'),
('0023936194', 'Afif Arifudin', 'XI.IPA.OK', '567a8b7d7685cb78361d0f73428e77de'),
('0027970317', 'Afifa Nur Fauziyyah', 'XI.IPA.OK', 'f1dbc4e05f1c8d76ae06e1a888ada71f'),
('0035840128', 'Alfin Maulana', 'XI.IPA.OK', 'f011fee93e0593c4539d960f5788bf2b'),
('0020153184', 'Anis Nuraini', 'XI.IPA.OK', 'e93311172abd032ecc63c7b7f6d1f872'),
('0027364244', 'Arif Ramadan', 'XI.IPA.OK', '1ad46a8bf0f5a37cd850e273d89351a0'),
('0012107901', 'Ayu Syifa` Mubarok', 'XI.IPA.OK', 'f088edcda458d171b5957bf4eed6337b'),
('0012410509', 'Bintang Purbalaras Adhi Putra', 'XI.IPA.OK', 'ad13b817df1121068708f459267468e0'),
('0011866444', 'Budi Santosa', 'XI.IPA.OK', '44c1d9862dc1867670b53ab8144c5632'),
('0024169903', 'Dian Nifani', 'XI.IPA.OK', '60591fc08dc5d45e4619d081c79d7bbc'),
('0007056053', 'Elinda Yunitasari', 'XI.IPA.OK', '23ba82ee5dbf0ab2c0403470a152017c'),
('0023991420', 'Faqih Rifa`I', 'XI.IPA.OK', 'd1ac4152918332b055851b583443b867'),
('0014644789', 'Faradila Nadawidad Ramadhani', 'XI.IPA.OK', '253ba7b51ea2fa1d0ff945d0cc1840ec'),
('0011864238', 'Hanan Farhana Sani', 'XI.IPA.OK', '1c72fcc3cfc2410b727668b029f1a6e0'),
('0002169731', 'Ifan Nur Faizal', 'XI.IPA.OK', '9600dc64923612607436df5191df138c'),
('0023970803', 'Imam Setia Pambudi', 'XI.IPA.OK', '5a2345994b63a39e2e14fb85bfa41019'),
('0023970862', 'Inayah Mauliya', 'XI.IPA.OK', '9d37ca849cf001a38a83dd967dcbfa65'),
('0021205383', 'Jeni Febrianti', 'XI.IPA.OK', '4ace5ca0b8187829a49955e30868d721'),
('0033621160', 'Juli Nurul Fatimah', 'XI.IPA.OK', '8915e174176460244c258865d86488ce'),
('0017830475', 'Khusnaeni Rahmawati', 'XI.IPA.OK', '303c3b219b7196f393c5315300c036c8'),
('0019338038', 'Kurnia Agustin', 'XI.IPA.OK', 'd104c3507f07be00fd30af2fa46c823c'),
('0017830528', 'Laela Mubarokah', 'XI.IPA.OK', '8ff419690b17a449b9ca329beca3d152'),
('0024877393', 'Lucia Kristianti', 'XI.IPA.OK', '6db39d6c19504b2253f1afe2b1df2497'),
('0024271656', 'Lutfi Oktafianingsih', 'XI.IPA.OK', '6f6f0736496b2cc9a3b13aaa1f866538'),
('0027709707', 'M. Haris Hasyim Ridho', 'XI.IPA.OK', '079362fdd5d865dda8bc40d7e3c06c2f'),
('0010827148', 'Muhammad Fatwa Rizqi Alfarizi', 'XI.IPA.OK', '0fa2acee75d258bf4cc910b3590ddb12'),
('0023970090', 'Maya Widiartiningsih', 'XI.IPA.OK', '2acf60eed133632a5a8a79521e095a1a'),
('0024239295', 'Mohamad Khafi Yulianto', 'XI.IPA.OK', '353db138e5adc6480ba9e1357b9a3e6a'),
('0034494839', 'Muhammad Febriansyah', 'XI.IPA.OK', '133551a1b198048f50853a146c872316'),
('0012458161', 'Naufal Mudrick Wicaksana', 'XI.IPA.OK', '59117df3e44930c7ebcf36494d7c72d1'),
('0011899154', 'Norma Enna Mandela', 'XI.IPA.OK', 'be450bc3aa9c6a35cb46279df1a3a15c'),
('0017050104', 'Novian Widy Subagyo', 'XI.IPA.OK', 'afa0e26fe1f36ef248ca50b3b1d4b0ae'),
('0024847032', 'Nurul Hidayah', 'XI.IPA.OK', '96d66ecf6d4f21a9dc24626c5e4456c7'),
('0024367124', 'Ririh Rigen Miranthi', 'XI.IPA.OK', '219e9ee343cd9215a2ef53ee1c96e111'),
('0002585267', 'Riska Dwi Afriliani', 'XI.IPA.OK', '798045c420700245451fc8dba9b586b8'),
('0023477561', 'Siti Ida Setyaningsih', 'XI.IPA.OK', '7612538d0e1eec507eb74e5a476a804b'),
('0017853092', 'Suprihati', 'XI.IPA.OK', '106e33af587cf1ce2c39f130e33847ee'),
('0021588061', 'Tiyas Rizki Saurina', 'XI.IPA.OK', 'c7c414fae8905a4b9b884fb50bbc6c35'),
('0012004690', 'Tony Alifyan', 'XI.IPA.OK', '9e07235ea20c2a9afc702c5a3db994f7'),
('0011767040', 'Wahyu Septiyani', 'XI.IPA.OK', '485fd01033cd1c39ab22cf56c4e3b9e7'),
('0028388023', 'Wiwit Septiani', 'XI.IPA.OK', 'da335e1d7e84a4ea099e08a72ba10170'),
('0024115802', 'Yusuf Adi Pratama', 'XI.IPA.OK', '0b70fbd824d7f085142431f307570802'),
('0017830495', 'Yusuf Baihaqi', 'XI.IPA.OK', 'a785e8701b344438495d7c7aa1e0a7db'),
('0023858122', 'Zakiyatus Sofia Aprillia', 'XI.IPA.OK', 'f07dc9e3f747bf351df9556bdf4dcb71'),
('0024250378', 'Adiva Febriliani Nuraida', 'XI.IPA.TB', 'cecc6f55a61dac9c31284416242df24f'),
('0020463248', 'Afa Azkia Nida', 'XI.IPA.TB', '42f85e73d60da09a2ebbb3e91beb9274'),
('0029660927', 'Ainun Nisa Nurfadhila', 'XI.IPA.TB', 'f4e0a9d1e6799511d0dd41bb7db952af'),
('0024239389', 'Alifia Ade Riszaeny', 'XI.IPA.TB', 'fb76fd0de7c09da3797d282c5232a1f7'),
('0028308304', 'Amalia Nazilaturohmah', 'XI.IPA.TB', 'e3e1788d4f112b142faaaf16a9215e99'),
('0017359407', 'Anggraheni Puspita Sari', 'XI.IPA.TB', '6d6353d7d9ede70c71eee551e27fda9e'),
('0023934987', 'Anisa Fitriyah', 'XI.IPA.TB', '2c27a32ae4d22799d3d99ba46f2e5b0d'),
('0029830068', 'Anisa Nurifah', 'XI.IPA.TB', 'ce4e01ea83621c9fde212cd0d6f49ade'),
('0021206047', 'Aprilya Ovta Viani', 'XI.IPA.TB', 'c0aaece47df6dce786327e1f0a6335c8'),
('0020741003', 'Cahya Ningrum', 'XI.IPA.TB', 'a25fe35bf615fd258d28a32be182d777'),
('0028099455', 'Dani Ad`Ha Feriana', 'XI.IPA.TB', 'cd24a150bdc04f51311f0d99bdd40bca'),
('0011878831', 'Dewi Yuliani', 'XI.IPA.TB', '552c59a34a31cdeb06d16ae7dafa181c'),
('0028099454', 'Dila Natania', 'XI.IPA.TB', 'eee0cd77d9420866d7712d528ba9e1d8'),
('0020740929', 'Eriska Neli Utami', 'XI.IPA.TB', '1e452f9fc67d37f4b867eada9ba369f7'),
('0018174668', 'Esti Dwi Safitri', 'XI.IPA.TB', '3af082f4f0c2a3ff163c5892b535fdfd'),
('0020728527', 'Fahrina Puji Aulia', 'XI.IPA.TB', 'e6c157591700c654ae76eabb30d73c7f'),
('0024115483', 'Fani Listiana Dewi', 'XI.IPA.TB', '4a583ba9db7b11a61ef49f155d8e6ddd'),
('0012538914', 'Faridhatun Solikhah', 'XI.IPA.TB', '746b9948ad608e5459b09f805605e91f'),
('0017554826', 'Fia Rosia Ningsih', 'XI.IPA.TB', '6b11b88d1e658bea3c58e66e2cea177b'),
('0027048736', 'Harumhar Dwi Salehah', 'XI.IPA.TB', 'e0cd690cfe44b21a358c17d11d792e51'),
('0010929123', 'Hikmah Tri Utami', 'XI.IPA.TB', '923795d27a39b26fe69be1b220ced276'),
('0020728519', 'Hikmah Widiyani', 'XI.IPA.TB', '10f146f0789f86052ccce381e24cc577'),
('0015664608', 'Husnul Khotimah', 'XI.IPA.TB', '8afe0e4bda49be18b58bc4c9be856739'),
('0021301826', 'Intan Putri Salsa Bela', 'XI.IPA.TB', '3236bd87c92e986d86533159fb57bfe4'),
('0024115813', 'Isnaeni Nurkhakiki', 'XI.IPA.TB', '848702fd1d05a11235d91a8ed0df0417'),
('0012006368', 'Kamila Sajida', 'XI.IPA.TB', 'e935746dd7b669a6576ca509ab185a03'),
('0031076421', 'Leli Amiliah', 'XI.IPA.TB', 'aa02b995b72cc3d8f457953f99a38a54'),
('0026305238', 'Lisa Amelia', 'XI.IPA.TB', '677866584f0d10f50cb3fa8ff6bd6a7e'),
('0022425227', 'Lisa Septiana', 'XI.IPA.TB', 'f57e45934425e315bfb7db4cdc706ee2'),
('0017830377', 'Muthia Fatikharani', 'XI.IPA.TB', 'a778d3f2343287566f936899168f4536'),
('0034712454', 'Najfatul Fitroh', 'XI.IPA.TB', '84ea709a130aafdac56722b5d80a0f03'),
('0003638641', 'Ria Rahayu Hardiningrum', 'XI.IPA.TB', '06130f5bd903a71e6a62d841d5fc47d2'),
('0017554828', 'Rindi Setia Lestari', 'XI.IPA.TB', '8f96f54c52eb78390ae0de65fa20f815'),
('0024239297', 'Rosyana Dwi Wulandari', 'XI.IPA.TB', '84de2348fa059d0248e487b24ee9443c'),
('0023260613', 'Safira Auliya Rahmayani', 'XI.IPA.TB', 'be48c7b567897c3b79910ea0252cf19c'),
('0023971667', 'Safrina Eisa Ilmana', 'XI.IPA.TB', '7024e8ca5646625372cc3dbb97d71a97'),
('0017853371', 'Siti Latifah', 'XI.IPA.TB', 'e3ce4dda4ff0522c7550cabcb502465a'),
('0022284242', 'Sri Rahayu', 'XI.IPA.TB', '5fa4217220c3d37ac62542a42f51841d'),
('0017830492', 'Syaifa Asri Prastiwi', 'XI.IPA.TB', '31e19790b8920e3f67ccdda5421ec1c7'),
('0017318004', 'Syofia Okta Amylia', 'XI.IPA.TB', '828d33b86fab4a08d266aad4225155c0'),
('0027509689', 'Wahyu Mega Utami', 'XI.IPA.TB', 'ef502ed7f76b39450313ab82207ed3d9'),
('0023858153', 'Zachwa Nurul Wakhidah', 'XI.IPA.TB', '19cff495a6bb40975ae624a488184c78'),
('0023753377', 'Adam Maulana', 'XI.IPS.1', 'e001a3dd8569191039e47e43d657da51'),
('0018159820', 'Aji Pengestu', 'XI.IPS.1', '25c9330c78a3078488cf45a8310000ba'),
('0010091648', 'Akhsan Prasetya Bhaskara', 'XI.IPS.1', 'f9e35255d30b1f721e4ebdd4b9f8c1b2'),
('0012535256', 'Amelia Felan Dwi Pradista', 'XI.IPS.1', 'a94f5f07359cf5414afe7ee1eddc601e'),
('0025701490', 'Anang Dwi Julian Prabowo', 'XI.IPS.1', '3992e95c291788aae7138049dab44016'),
('0020299456', 'Arin Friska Lujiana', 'XI.IPS.1', '915c76bde4b7f39501deec60fa7e5506'),
('0015646755', 'Buyung Tri Wahyudi', 'XI.IPS.1', 'f96a4c3cb10ce704fb348220e1b8619b'),
('0005664187', 'Danu Jaya Pratama', 'XI.IPS.1', '71922045efaf339967c42c7b7eaed117'),
('0024239554', 'Dwi Astuti', 'XI.IPS.1', '2f55e0f84e9133d86de138f4c87652ce'),
('0024239474', 'Dzaki Hibban Reswara', 'XI.IPS.1', 'e24e2865c94dd6d818258f7f4f8518b5'),
('0017074334', 'Fadhilah Nur Shiyami', 'XI.IPS.1', '6d8ec6a88ff3c98b34a21ba856946506'),
('0028011873', 'Fahresa Mareida Prasetya', 'XI.IPS.1', '78fea52b3eaf1af20c7275eed6762de5'),
('0028162509', 'Fakhri Syuhada', 'XI.IPS.1', '42f44fa99a79287f742432f2295f5614'),
('0017776001', 'Fauzian Akbar Mualip', 'XI.IPS.1', 'f182e7e1e2d0a44ee2a8d6018b151ae8'),
('0024239273', 'Hana Indah Pratiwi', 'XI.IPS.1', '2418dd3112b8d7ea274cdf83bd722502'),
('0013973475', 'Hanif Amin Muarif', 'XI.IPS.1', '3e3d6f6fb128778549ba856d9f682f46'),
('0022305938', 'Ibnu Rhojas Fahru Rozi', 'XI.IPS.1', 'b79e2b396aa28e8db3e76da891862803'),
('0030316124', 'Indah Nurfatimah', 'XI.IPS.1', '10ee1f597d8d46051c3b498ef0f950f5'),
('0001054498', 'Jefri Martin Adha', 'XI.IPS.1', '70149c24b64890d235ea38018ba117f6'),
('0029977912', 'Laeli Tri Fatimah', 'XI.IPS.1', '73cea7a117d516e3ac3c4f7f477e1de3'),
('0023001076', 'Lukman Susanto', 'XI.IPS.1', '433e707ff507ee2f411a6d4b6d08c7cc'),
('0017337224', 'Muhammad Nur Aziz', 'XI.IPS.1', 'ff5ba8b1732984a450db11fc3a08257d'),
('0024099231', 'Muhammad Zalfa Nurrizquloh', 'XI.IPS.1', '02d6d81921134baedbcc47c9010a5135'),
('9999683844', 'Misno', 'XI.IPS.1', '4d8d43a9d7204a6991553a508e77fcd3'),
('0027972518', 'Moidhoh Lutfiyah', 'XI.IPS.1', 'e0c3504ed13b39b3f6959f2afe8eb22a'),
('0026549102', 'Mustofa', 'XI.IPS.1', '63a875b33ba28e6eee7422ad51eff31f'),
('0027634550', 'Naufal Almas Allam Fadlulloh', 'XI.IPS.1', '8bd7158f19bc18d56aeacb245ec4dec0'),
('0024272033', 'Nely Zhulfa Sofia', 'XI.IPS.1', 'f56590be0f395d9aa103afb19fdf97ec'),
('0020728511', 'Nurita Khomsanah', 'XI.IPS.1', '9d32f5f48faa98cf4bde1ec8f001890a'),
('0017515676', 'Raihan Zaky Nur Razaq', 'XI.IPS.1', '4f91cf63e0a3609f5cec87e2b405749f'),
('0020741367', 'Reyga Maulana', 'XI.IPS.1', '234243d43f356ddcff44c951bc5d7059'),
('0023970854', 'Rhisma Melfiyani', 'XI.IPS.1', '67efd8228baf6a4e56d05d2bb5265b30'),
('0028011845', 'Risma Risqiyani', 'XI.IPS.1', '7768b345f1a79dd255e4bbf09328e630'),
('0002166742', 'Sayyid Annas Mushaddaq', 'XI.IPS.1', '48b5d83cd56aaa4d0c7e063af07a850e'),
('0021687784', 'Shakila Yuan Wulandari', 'XI.IPS.1', '79d7b79fb5497dc381e30ae2ef715143'),
('0017328094', 'Slamet Tri Setio', 'XI.IPS.1', 'f832861a6274ba51570621ee084d8a7a'),
('0013707167', 'Tika Yuliana', 'XI.IPS.1', 'c2572074b1220f2edc59e782098287d3'),
('0020740914', 'Ulfah Nur Hakimah', 'XI.IPS.1', 'e417f76d8d005d30d6348de6b112817a'),
('0024271689', 'Watina Rahayu', 'XI.IPS.1', '9f70fc8d86363e1739ae72c5b0aec27f'),
('0023858054', 'Zika Ajeng Widyaningsih', 'XI.IPS.1', '52bf7ee7b6c433ea4e6b69ce3523c15b'),
('0028099460', 'Athallah Dzaky Rahayu', 'XI.IPS.1', '72f1da0b203ff223c9ad107d1bdc3ef4'),
('0005695509', 'Ade Restu Irmawan', 'XI.IPS.2', '4979f78ac7d139b2ce88ccd22d395be6'),
('0013669803', 'Aji Romadlon', 'XI.IPS.2', '07663dc902ddd761663cc0750f4e51cd'),
('0021487910', 'Amelita Febrian', 'XI.IPS.2', '517d9d215f01ad682d8d133e50bd4d00'),
('0011878810', 'Anisa Khoirul Umami', 'XI.IPS.2', '3850802ff1874809610485eb0390be74'),
('0024115867', 'Anissa Khusnul Hotimah', 'XI.IPS.2', '9883c298a8f0fc0d68dfe3a0f12e9fd7'),
('0024239569', 'Anugrah Yoga Pamungkas', 'XI.IPS.2', 'fcae0e8398bb5b887c418889cb875b71'),
('0019521036', 'Awit Elan Maulana', 'XI.IPS.2', '546b24ffe35af4564166eebb703903ca'),
('0020741359', 'Bayu Indra Winata', 'XI.IPS.2', 'cc11ede07c89f603402de24482c494b4'),
('0017074325', 'Bilqis Khairunisa', 'XI.IPS.2', '4cd71377250f112ba3dca65ea7a5208a'),
('0024501418', 'Dika Ananta', 'XI.IPS.2', '4c426395d4122b6119e55adc5b04b155'),
('0017679861', 'Dwi Agus Setiyaningsih', 'XI.IPS.2', '8123a5510164bcddd1889fc95697d54e'),
('0022657755', 'Dwi Rani Febriyanti', 'XI.IPS.2', '0b8592ecf7619618c770df0198b4656f'),
('0017831335', 'Ervan Miftakhul Huda', 'XI.IPS.2', '226c423eab407164a55df70c564668ee'),
('0029260264', 'Fadila Dwi Handayani', 'XI.IPS.2', 'cae2b78bd2fab88c4c6ef25b3883736f'),
('0017830319', 'Falah Helmi Nur Misbah', 'XI.IPS.2', '28138f25a905c09563fc867c78a5ef31'),
('0022346927', 'Gilang Dika Prayogi', 'XI.IPS.2', '1cd4b11fa355661537916c743e63952d'),
('0024271901', 'Hana Nursajidah', 'XI.IPS.2', '8d09dd6131df275830a5544fc5f14afc'),
('0025664970', 'Irwan Ardiansyah', 'XI.IPS.2', 'f404466174b0b2d0291ba1243b8aef31'),
('0023601092', 'Kharisma Nur Indah', 'XI.IPS.2', '9703c5bc71a5b08fa414fdba23585366'),
('0017077933', 'Lestari Endah Pramestri', 'XI.IPS.2', '6703fee43db1499534da79ac2b9432f3'),
('0010091900', 'Lusi Astianingrum', 'XI.IPS.2', 'ca887ddc7a783cda89ac08c04e0f153c'),
('0024098079', 'Lutfi Hikmawan', 'XI.IPS.2', '4f1237c4ecfc5070b947df5b0fbd9463'),
('0020683057', 'Muhammad Farid Nur Alam Syah', 'XI.IPS.2', '3180c3359b261861045192579315e481'),
('0008887067', 'Masriqin', 'XI.IPS.2', '4a194899a9390ec4e47474cab4bec5d7'),
('0023735330', 'Mokhamad Hamzah Muliawan', 'XI.IPS.2', 'a1cde43f54e132fefa39824836049d77'),
('0023988639', 'Mufidah Dwi Yulianti', 'XI.IPS.2', '4cd006b301a610291808eda97dc93039'),
('0017830465', 'Naf`An Ahmad Nur Rosyid', 'XI.IPS.2', 'db647e011741822479a0cb1aef8a3b7a'),
('0023970833', 'Nazif Adelintang', 'XI.IPS.2', 'e2a873827955fae5bb01250dc935d85b'),
('0002585230', 'Nida Zaidaturrokhmah', 'XI.IPS.2', '7d13d784f07cac908f13c7c31bd57eaa'),
('0030274979', 'Patricia Adinda Mutiara', 'XI.IPS.2', 'b6273ca658812e26de81565dc0afd66b'),
('0017050157', 'Rajif Jaya Fardani Sulaiman', 'XI.IPS.2', 'ef21e3679cf88a8b8d6869447448a9ee'),
('0017554820', 'Ridhlo Pahlawi', 'XI.IPS.2', '634d32ea1725bd59192ab8ca4dc61138'),
('0027780773', 'Rifqi Hairani Ahmad', 'XI.IPS.2', '35d49ca40fba24b71417b0a40742ec05'),
('0020321308', 'Risa Anggita', 'XI.IPS.2', '99fa8de29f213be2ee847b92b30f88f1'),
('0023858072', 'Rizka Dwi Oktaviani', 'XI.IPS.2', '03fc1899b40620bc5a83484205d9f0fb'),
('0038258387', 'Sekar Diah Wulandari', 'XI.IPS.2', '0a2bd50c083aff141c1d43b71c3fcf48'),
('0001054686', 'Septa Dwi Pratama', 'XI.IPS.2', '6975f6a41e8a93ea6f2172ed0ec54736'),
('0020860011', 'Sri Aminah', 'XI.IPS.2', '3a86eacdf7a742305729af91a0edb2fd'),
('0020683091', 'Syahrur Rifai', 'XI.IPS.2', 'a2ed3790d18a1138aa1843569039b4c6'),
('0011916497', 'Titi Sulastri', 'XI.IPS.2', 'ffbdab75185296918eb175bc6c6eca42'),
('0023158446', 'Umi Rodliyah', 'XI.IPS.2', 'e607049eb0751968304ee420cc345ef6'),
('0027670673', 'Zidan Febriansyah', 'XI.IPS.2', 'c8f061b4ef8f1aae99aff78ed00c261d'),
('0011664556', 'Adila Rahmania Izzati', 'XI.IPS.3', 'e69716cbb72f2689aa4a61d2cbf1b438'),
('0020154887', 'Ajrin Dhiaz Nur Pratama', 'XI.IPS.3', '8c44ff66d3772c2fc7079e986199945c'),
('0017624636', 'Alif Maulana', 'XI.IPS.3', 'af19c283c435c58e5b4bc54670725afc'),
('0023451151', 'Amalia Dhian Nurhaliza', 'XI.IPS.3', '4c366536502c7be295d033038b4eff61'),
('0024239227', 'Ani Rochyati', 'XI.IPS.3', 'ec83a38648b8a3ae4c00c90827daf1b9'),
('0011664874', 'Annisa Fikriya Khanafiah', 'XI.IPS.3', 'e8cddb9646697f64ec825f45b59c0a73'),
('0020669217', 'Ardika Dwi Rifqian', 'XI.IPS.3', 'd5fe79eef09cd8284b43283d2213495f'),
('0013832183', 'Azarine Phelia Huga Anuri', 'XI.IPS.3', '36c44587d35788874987fe08ac2c5384'),
('0028124105', 'Aziz Cahya Saputra', 'XI.IPS.3', '4c7ce84f9f03af28956c842b785d01f0'),
('0019396182', 'Dafit Dwi Prasetiyo', 'XI.IPS.3', '97f9b6fd24942961816c102fcc77e2ce'),
('0017830315', 'Diana Agustia Melasari', 'XI.IPS.3', 'c1d6f458c82c6276baf6c36a2d5a9a8d'),
('0031062917', 'Dimas Setya Wardana', 'XI.IPS.3', 'e2db1f51b9bb8b68e6f4507ef48de8ca'),
('0017830339', 'Dwi Yuli Utari', 'XI.IPS.3', '239cb1b84711ad34c219bda070dda69e'),
('0029777122', 'Fahrian Dwi Rizki', 'XI.IPS.3', 'fee25cec19735e1bf1c89ab67cfb9ab8'),
('0017318083', 'Farhan Puji Ramadhan', 'XI.IPS.3', '9b8b8298e922b9d20f4bc929e1113d74'),
('0024272040', 'Fitrotur Rokhmah', 'XI.IPS.3', '24addfcf865fc7669c2c1f46e44f3051'),
('0011873626', 'Harya Mahadewa Akbar Chicky Bharata', 'XI.IPS.3', 'a684ae7644be3720619a10166e981bb3'),
('0020685580', 'Hasna Dwi Maulia', 'XI.IPS.3', '0e4b9ac3dc14a7914f84d301e9cbc0cc'),
('0028928905', 'Irza Fauzan Rifqi', 'XI.IPS.3', 'e131d769b611a09502323b55a7a2c921'),
('0020527369', 'Isfi Ngaena Masfufah', 'XI.IPS.3', 'cd6f08718241c8561b9d8fc83d24c0c1'),
('0014540855', 'Khafifudin Khanif Miftakhurrohman', 'XI.IPS.3', 'a304dfc23424257f38c8dee5bd47b832'),
('0001055794', 'Khoelani Fitri', 'XI.IPS.3', '0b405ff65da9c9ac6c284f25530cba5c'),
('0015723859', 'Muhammad Rahmat Ilham Ma`Arif', 'XI.IPS.3', '9b5a162642f8263fd156ef755875c16a'),
('0023970764', 'Mufidatul Khoiriyah', 'XI.IPS.3', '24406870b8e2eec7594456afc80bc1c6'),
('0028424588', 'Muhammad Faldia Akbar', 'XI.IPS.3', 'd959e3c49e5c89594a2ae5e2474cbb38'),
('0027236509', 'Ninung Erika Ningsih', 'XI.IPS.3', '32129dfc601e3ceed79c8b7248cd955e'),
('0024239301', 'Nungqi Khamidani Annas', 'XI.IPS.3', 'da03e08423b36b40dadaf93cb8aec6a1'),
('0028011844', 'Pramaishela Nur Aunilah', 'XI.IPS.3', '230e8da6243a9b392c43bece468926da'),
('0020681994', 'Risa Muswanti', 'XI.IPS.3', '437097a4e99173e5fc06e7aa9af3881b'),
('0017499950', 'Rizal Zain Akbar', 'XI.IPS.3', '1f5bd96a55cf1573327efa953d8fb1d1'),
('0024271650', 'Rofi Yusrita Khusni', 'XI.IPS.3', 'f35ea867d99650924cdbcb4a9af6fbaa'),
('0017853313', 'Sekar Maharani', 'XI.IPS.3', 'f9852490c21a7e3b5742cca4bd82c1c2'),
('0006940370', 'Sigit Kurniawan Saputra', 'XI.IPS.3', '077c2c6b043120c43a440badd247a4f2'),
('0020728529', 'Sri Jumiati', 'XI.IPS.3', '55d515153d0b6fdeed2b5d69744e113f'),
('0023451473', 'Tri Utami Wulandari', 'XI.IPS.3', '239d044ed404ce508e24219febc06f19'),
('0024272043', 'Uswatun Khasanah', 'XI.IPS.3', '8815bcb725dff7061fa32925afff27a2'),
('0029790636', 'Wahyu Ngalim Nurrohman', 'XI.IPS.3', '447ad3b4765d9c95c3059f35aeedae1d'),
('0024271919', 'Wulan Listiana Prilianti', 'XI.IPS.3', '4ccb15c12a0d23e509cd15ed1a87a1f1'),
('0017830488', 'Zanuar Rendy Mesa Syahputra', 'XI.IPS.3', '32b6ccbf987c94b1e6ca940e98ceee90'),
('0011874217', 'Agung Setya Utama', 'XI.IPS.4', 'd0b58eafb74d8720752056a552a97219'),
('0011878730', 'Akhmad Nur Kholis', 'XI.IPS.4', '74870ccdf5f3fe5dfdc9e21d694fe7db'),
('0020156431', 'Akmal Basyir', 'XI.IPS.4', '85f0b8bef3bcddb9e3f8ef2c866198b2'),
('0015028742', 'Alip Khozanurri Rizqo', 'XI.IPS.4', '95ad36e2bc2420507ce21605830a43aa'),
('0029181592', 'Anggita Lestari', 'XI.IPS.4', '470f7cb2741378b447ba8a10b3660779'),
('0025067253', 'Arif Alfian', 'XI.IPS.4', '08fc7dd98d8d54b4fceb8f4e5158cbd6'),
('0012107917', 'Ariq Alfa Dinillah', 'XI.IPS.4', '289d069802781bdaf41228250a59a623'),
('0023970480', 'Bagus Prianing Budiman', 'XI.IPS.4', '2c78445c825e66552d9b6eda82cb8639'),
('0032302820', 'Dandy Jayus Setiawan', 'XI.IPS.4', 'ef2f3b61539126bc329247b35ed8d60e'),
('0024271641', 'Dini Wulandari', 'XI.IPS.4', 'e25d4d7fd2123bf110c258591c702d31'),
('0001036878', 'Dwi Galih Prasetiya', 'XI.IPS.4', '2255d2b4c77c1266b963c15e8e11b1bb'),
('0017515690', 'Dwiki Nur Ilmi', 'XI.IPS.4', '8b4233b609d6e9a2bbe0717cd4790acb'),
('0027972522', 'Fajarubi Nur Fauzi A', 'XI.IPS.4', '75ecd40fb37aec67912140223d19b726'),
('0017831296', 'Gafifasola Naufalia Hanif', 'XI.IPS.4', 'aa295563548bdf505878c085b582cd05'),
('0011874724', 'Hafizd Erlangga Wiranata', 'XI.IPS.4', '99d8af9bd183f0538278c5fe79813037'),
('0010827179', 'Ibnu Maksum Izul Haq', 'XI.IPS.4', '890ed9a6a4dd6af327a68c725e60867c'),
('0033335818', 'Julida Mahendri Yayang Selviana', 'XI.IPS.4', '3a8a365e762118a80b5053634f1fb531'),
('0024271911', 'Khairina `Aisya Rahma', 'XI.IPS.4', '2ee1df3c88000ba8d982d693e32b85c0'),
('0017317954', 'Kurnia Dwi Lestari', 'XI.IPS.4', 'c69de233957961e9936392058fb3faba'),
('0006631724', 'Latif Nurhuda Erfin', 'XI.IPS.4', '7824b32203953716a08fd1fa3d6428cd'),
('0011898975', 'Mohammad Jemy Akbar', 'XI.IPS.4', 'bf6a5a84bafc57b8a93311b20baae464'),
('9991618161', 'Muchamad Yuda Pratama', 'XI.IPS.4', 'eff0275e23ec40d3ae5f8173612824fb'),
('0024115641', 'Miftahul Huda', 'XI.IPS.4', '7ae7f735ab776b8a528d8d948d027b76'),
('0023897472', 'Muhammad Ilham Machfudz', 'XI.IPS.4', '78a05aa4d0f70d08e6ee4f752d6602fe'),
('0017475730', 'Nanda Rafi Hufron Athorif', 'XI.IPS.4', '63ee4bb7453362522dc712434688874f'),
('0023783101', 'Nanik Purwaningtyas', 'XI.IPS.4', '30f60a2660f42fb91b062d21cda97d0f'),
('0026484860', 'Nur Fahyu', 'XI.IPS.4', '8cf55cd1d502a5e7fd60a94e4403b70b'),
('0030275237', 'Patria Nur Hidayatuloh', 'XI.IPS.4', 'a46396d240b0598a27a394360cf386c9'),
('0030316142', 'Renandra Kevin Pahlevi', 'XI.IPS.4', 'd377880da70f3e303e013455cbe0b1d5'),
('0029957080', 'Riski Nur Fadilah', 'XI.IPS.4', '39ec83e568cef0c71d5355f30e904aa3'),
('0027990646', 'Rissa Dewi Anggraeni', 'XI.IPS.4', 'e5a59ca7aa29f50924a387f812bcf96c'),
('0030311488', 'Rizki Afrianto', 'XI.IPS.4', 'd7e77e8e1d11502f9d1990ed3172697f'),
('0023066661', 'Septia Arum Sari', 'XI.IPS.4', '74ef0e5c1fa07d7c317fe4a9b3778130'),
('0023970459', 'Sigit Saputra', 'XI.IPS.4', 'b082f46f6e97d0301da20f5cea7e2afb'),
('0024239472', 'Tasya Putri Nabilla', 'XI.IPS.4', '79225ad527a5674e8a64ec20d2798dec'),
('0030338026', 'Tunjung Lidia Widiastuti', 'XI.IPS.4', '8c57554e5f2ddc2428200a5adcea1ab5'),
('0011874808', 'Voni Andriani', 'XI.IPS.4', '14ee9181be9a029823da3a27e072070e'),
('0023475278', 'Yan Swesti Ivanora', 'XI.IPS.4', 'fcaaf5ec445b22b1ff71955056872d4d'),
('0024115851', 'Yeyen Rindi Monika', 'XI.IPS.4', '7eb8c62b90e53c40fd81b197a263dbd6'),
('0012505743', 'Yogi Cahyanto', 'XI.IPS.4', '63a62df71aaaced978fdb5d93115ffc4'),
('0019847920', 'Yuliatun Hasanah', 'XI.IPS.4', '64a4caffa705b928570ced58ac345dcd'),
('0023129397', 'Zsa Zsa Azzahra', 'XI.IPS.4', '89c88a29ce66ed09d0dce6a12d5f35c9'),
('0034685601', 'Afik Selina Nur Faoziah', 'XI.IPS.OK', '7b73f128bff0b2ee4ddc946db1d92435'),
('0017830337', 'Ayu Fitriana Lestari', 'XI.IPS.OK', '17a8cdac98dd15599ab401a20116fffe'),
('0018409731', 'Diffa Nuraga Ramadhanis', 'XI.IPS.OK', '3af03a5a996ac1dd94f445b22add3e7f'),
('0020681912', 'Esa Juwita Alvianti', 'XI.IPS.OK', '3339cec4193f0d56e2234dff841239d5'),
('0023730638', 'Fachreza Reyhan Abiansyah', 'XI.IPS.OK', '809f9fead02e4b2d3e4e0b3f6bbaf8ce'),
('0028849733', 'Fadhilah Putri Arliana', 'XI.IPS.OK', '23f9382630034a255ceb6887837e0090'),
('0020669838', 'Fadilah Nurma Andriasari', 'XI.IPS.OK', '2ac01ebc3614a8372299b302258f67a3'),
('0027972526', 'Fahmi Hanif', 'XI.IPS.OK', 'd163d3822781cd601af55e13d9cdd9fc'),
('0023185459', 'Fatimah Nabila Azzahro', 'XI.IPS.OK', '9a2bec5a397e4e9e683e624e030bd02b'),
('0017830320', 'Hakim Mahendra Ar Rasyid', 'XI.IPS.OK', '5dc7f81d473551cbdac9103f7f3362f9'),
('0024097152', 'Hindun Vila Mudrikah', 'XI.IPS.OK', 'fa2f7c69bc8d8381f2ddbd1af81998bc'),
('0023475276', 'Huda Abi Rustono', 'XI.IPS.OK', 'ea17cfab3104b0637df3ce0505329b42'),
('0011902145', 'Iliyani Eka Rahmawati', 'XI.IPS.OK', '947b299da64ae06e1e954a5cf990acbd'),
('0023182428', 'Intan Febriantika Hidayati', 'XI.IPS.OK', 'f2a21efb5dbac1b94392bf002106f839'),
('0017676002', 'Iqbal Fathur Rahman', 'XI.IPS.OK', '4644be1e8eaa61d7d09a0f3a3e4394a9'),
('0034066476', 'Irma Yuningsih', 'XI.IPS.OK', '00cf817efe87d9ffe1577755537d9acf'),
('0023858088', 'Iswara Ratri Hapsari', 'XI.IPS.OK', '21304da1f0610fd92a76ddf805778c85'),
('0023451132', 'M. Andhika Yusuf Ezira', 'XI.IPS.OK', 'cb70dca845d21727d2a211dd04b573b5'),
('0030318245', 'Marshah Azizah', 'XI.IPS.OK', 'a06dec3950b9b3d52fdc12719e0d86f4'),
('0029636515', 'Misriatun', 'XI.IPS.OK', 'a7464ba34cdf8d7302330924cd81cac4'),
('0024239288', 'Muhamad Nurul Ady Tama', 'XI.IPS.OK', '87e83d8242a422f481be6b0186808e7d'),
('0017830494', 'Muhammad Zainudin Abrori', 'XI.IPS.OK', 'dbf5660bc341ed56152384a98ec3d5f8'),
('0028190832', 'Noer Falida Amania Ummu Habibah', 'XI.IPS.OK', 'c6358a1e1d03dd25d8d380d32c5b5753'),
('0029640903', 'Ragil Saputra', 'XI.IPS.OK', 'f5216fa9152fcaeca7184e3b680362fa'),
('0011878829', 'Riki Juliantoro', 'XI.IPS.OK', 'd946e5f639f8ee57a41d157a6ff0388c'),
('0024272018', 'Rina Rahayu', 'XI.IPS.OK', '9e640716f93428276ebf6872795fd191'),
('0029304932', 'Rizal Subakti', 'XI.IPS.OK', 'a7ecdf6f2f773df0011fad7ba4682785'),
('0023858065', 'Sabilla Zidni Amalia', 'XI.IPS.OK', '63ad1c63d5ebe636ac819199006521f5'),
('0023126461', 'Siti Elsya Awaliah Windi', 'XI.IPS.OK', 'de9cbdb18b30aae13202fc856738e7c2'),
('0024239270', 'Sulfian Putra Pradana', 'XI.IPS.OK', '52a8c9e46f2615a0668d135400dae6df'),
('0023970809', 'Tofik Kurokhman', 'XI.IPS.OK', '11dcf0b557f41ff726ae66ef8ccdc15e'),
('0019673015', 'Uvi Auliana', 'XI.IPS.OK', '8a585c477dd6b8a1b2c4a10fb56d1fc0'),
('0011350845', 'Wahyu Andika Maulana', 'XI.IPS.OK', '8807bc0e2efab57ea6e7a69b949f556f'),
('0020527374', 'Wiwi Pratiwi', 'XI.IPS.OK', 'b7ad0edb6a04cb4ac2be47845185e47b'),
('0038258305', 'Yazid Akmalul Fatkhi', 'XI.IPS.OK', '0bfbbaf73a993268a961de851eb8b28f'),
('0017554693', 'Yoanda Fitrah Ramadhanti', 'XI.IPS.OK', 'fd2685d6f7048fb87d98196ca1d98d98'),
('0017655381', 'Yufa Nazal Rhamadani', 'XI.IPS.OK', '7b8bdc075b26364b7dfc441188387659'),
('0024115452', 'Yusuf Ade Saputra', 'XI.IPS.OK', '953fa640e3b01252860bdaca213528b6'),
('0021044252', 'Zada Titto Erlangga', 'XI.IPS.OK', '5a5a12f3469f3f42ea9551d314018baf'),
('0023970845', 'Zahrah Qurrota`Ain', 'XI.IPS.OK', '3b1cfea7028bb9de39e9c39f984c2af5'),
('0023475161', 'Zulfa Istighfaril Hidayah', 'XI.IPS.OK', 'ca40c0b8e752fba2e6b351e9390def40'),
('0023046844', 'Aprillia Nurhalizah', 'XI.IPS.TB', 'f3f662bf889b7aff0d5619c22fcfaef6'),
('0024622405', 'Avilia Reni Purnama Sari', 'XI.IPS.TB', '32eaeb05aebb41997ad8ee2630f0457b'),
('0020680001', 'Ayu Puspita Diani', 'XI.IPS.TB', 'be9468c4677d92b161e5750af959d479'),
('0023897575', 'Berliyana Nur Indah', 'XI.IPS.TB', '1b2d865dc68181e580cb3e7e3e2be157'),
('0023959981', 'Dian Nur Andini', 'XI.IPS.TB', '0134acec273305a39bb7cc1ba8fe0650'),
('0017554827', 'Distya Nurvitarani', 'XI.IPS.TB', '3cbd61702cb4613404eac41a7161d72a'),
('0002585272', 'Dona Mahdalena', 'XI.IPS.TB', '8a98bc7f03742a1f17dec1ac444e59a5'),
('0030294526', 'Dysta Septiana Putri', 'XI.IPS.TB', '9df305349a2166221a11a39d57deb312'),
('0014788352', 'Elya Tri Yuliana', 'XI.IPS.TB', 'de58d0e0651ef91cdbe28de9e6a87839'),
('0017515902', 'Fika Trianna Rahmadhanni', 'XI.IPS.TB', 'cb52256730ef75778044f25519a6fb65'),
('0005695270', 'Hafiza Desfiana', 'XI.IPS.TB', '3f158e39908f39fbbc8da332f1f60b12'),
('0011875233', 'Ika Yuniarti', 'XI.IPS.TB', '15009ddcc17ad917c95eab7a3c72ba2d'),
('0023735312', 'Koes Rochania Amali', 'XI.IPS.TB', '01aa0ff7d8df04fd56ab2a6eb7468dca'),
('0017554756', 'Linda Nur Hanifa', 'XI.IPS.TB', '246aa5b72a29ec6143b1ab513ab8e8f2'),
('0011864071', 'Marvungah', 'XI.IPS.TB', '5b252a1188a94d2d08b60a9c2fad5254'),
('0024239399', 'Natasya Salvatirana Nur Saputri', 'XI.IPS.TB', 'bcf0d67f089f7e970dd48763c67a14b5'),
('0024877410', 'Nova Nur Amaliya', 'XI.IPS.TB', 'a44c9be183fd2a2204f7479401f69663'),
('0016176565', 'Nova Nur Rahma', 'XI.IPS.TB', 'f55ef5615f7b08bee2edb503d6db7641'),
('0017050102', 'Oktiana Nur Faiza', 'XI.IPS.TB', 'f352bab7de90be4a766b611b557465eb'),
('0002143717', 'Regita Septiani', 'XI.IPS.TB', '966a662ba82ed52a6d2dc136e0413d43'),
('0038025043', 'Sekar Wiranti', 'XI.IPS.TB', 'fbcb4eb30d14090daab6bd86e1253b82'),
('0024271915', 'Setiya Riski Febriyanti', 'XI.IPS.TB', '995f6c2d2c4de66723e97551d03da8c4'),
('0024239562', 'Tita Nia Maudi', 'XI.IPS.TB', '9bcf9188b1546ae2f03ab0d4f1559ee6'),
('0001055551', 'Tri Indri Widyaningrum', 'XI.IPS.TB', 'f110d6b91caa673d4dfe20b32feb926e'),
('0028842478', 'Vika Sucianti', 'XI.IPS.TB', '74f2465d9d0e046ab8f9978ef30218c9'),
('0026026549', 'Wulani Asiah Khasanah', 'XI.IPS.TB', 'cf3f5394a05c141ff33f3c05fb708d5e'),
('0024237143', 'Yayi Diana Dewi', 'XI.IPS.TB', 'ed9d8df63845b9e092f8b39393f8ba5b'),
('0023970846', 'Zahra Astrid Alifka', 'XI.IPS.TB', '0ac406ec822717dfd302958f31506c0e'),
('0018842262', 'Ahmad El Mu`Tashim Billah', 'XI.AGAMA', '87f2f4fbf2ef3d380c9f47f5fe655d22'),
('0019809621', 'Ahmad Rifdatur Rozak', 'XI.AGAMA', '89fb6e25fc2bda18874bf9ebb1784547'),
('0017830481', 'Akbar Widiyanto', 'XI.AGAMA', '584b13dfa5c559faea171e47e5cbbfb9'),
('0024239296', 'Akhmad Subekti', 'XI.AGAMA', '9431a5d8e3e6fcbf9cdd7017458f65df'),
('0026403660', 'Annida Ul Khasanah Kalibening', 'XI.AGAMA', '8b24b110dee6dc9bd89e72af6d4a7469'),
('0010724169', 'Annisa Lu`Lu Ul Fitroh', 'XI.AGAMA', '1dea8ebee6eadf905799a3fb52328738'),
('0006804485', 'Arief Budiman', 'XI.AGAMA', '963d69cfa5b31e4136a6fc833a5a57cf'),
('0024888130', 'Desi Safitri', 'XI.AGAMA', '454c03eb8e0c2782bf5432d15ad8aebd'),
('0020682010', 'Denny Yulloh', 'XI.AGAMA', '19c987b67ff031d8f3fbdfd256902028'),
('0017853224', 'Desi Fitri Salsabila', 'XI.AGAMA', '812da130c7780c88f25b200b61db2618'),
('0023615789', 'Destha Annas Istiqamah', 'XI.AGAMA', 'ed43028833fc0feba95a0f979855ccce'),
('0013503460', 'Desy Nur Fitri Sukma Pratama', 'XI.AGAMA', '1d46f8570c90696c0414ae045e676048'),
('0028099491', 'Dian Mukti Handini', 'XI.AGAMA', '1b0fe050ced53b161172df0764471cad'),
('0027460867', 'Dwi Setyo Pambudi', 'XI.AGAMA', '4fdd0c30858e60597d613dcca9721f3d'),
('0025321538', 'Elta Dwi Prasepti', 'XI.AGAMA', '68b0b08c1a003497f0578cfacaef2020'),
('0019627688', 'Endang Setianingsih', 'XI.AGAMA', '3428f39d57ea9cb55db01e2db73a7c8f'),
('0024115549', 'Evi Nuriyah', 'XI.AGAMA', 'ef053d24d806fb5280867c02b22586c7'),
('0025041672', 'Fahriza Saputri', 'XI.AGAMA', '6b3ac53291fca98b93ed0260ed74ae2e'),
('0024283056', 'Farhan Nur Hidayat', 'XI.AGAMA', 'ff891cc1c196358b08c4a45888a20930'),
('0017830392', 'Ferdian Rajib Liga Anggit', 'XI.AGAMA', '9e8b9f840a0641e0fa6d64035bb8067e'),
('0022025819', 'Fitriatun', 'XI.AGAMA', 'd60f3922e524cd1f48e36e95077cb665'),
('0024298097', 'Ibnu Rasyiid Al Ghaniyy', 'XI.AGAMA', '97e905ea36cda119544782539a0047fe'),
('0027674395', 'Latifah Diah Palupi', 'XI.AGAMA', '8b6c30f5c6ba724abccf0043ef782e1f'),
('0017074331', 'Lisa Kinanti', 'XI.AGAMA', '80102e0130c038080e1eb89571f9d325'),
('0019763132', 'Mastna Rofiqotun Nisa', 'XI.AGAMA', 'd4f76d2ba58ad2f8dbd707c797aee0f7'),
('0024239269', 'Maulana Daffa Dwi Saputra', 'XI.AGAMA', '5af1b2b68a3a3051a2f254c83c04111f'),
('0023970676', 'Meta Ananda Rizki', 'XI.AGAMA', '4658fe6e84205e3cb7d30f2147e1f5e5'),
('0020728722', 'Muayadah', 'XI.AGAMA', '11594803a8f2ee4147097e2aece805b5'),
('0023456918', 'Nelli Muflifatul Jannah', 'XI.AGAMA', 'ee36020d51262250da4d8a8a3094f3f3'),
('0021227826', 'Nida Azizatu Roidah', 'XI.AGAMA', 'b55292f6368e83d98630578568a86f99'),
('0020680007', 'Novisha Aprilia Sukma Nanda', 'XI.AGAMA', '437ff0ce86ecafd32893c581c7b54f91'),
('0017533981', 'Raihan Sherlina Rakaputri', 'XI.AGAMA', 'dd7196ba1d83399d518e677eed80b810'),
('0023617108', 'Rio Fauzan Ikhlas Purnomo', 'XI.AGAMA', 'd0b478a1809da7d722a34a47b3b2b65f'),
('0017072688', 'Rizal Firmansyah Musyafa', 'XI.AGAMA', '4b2afb8f1ec91525520adbd3bddf8f0f'),
('0005723831', 'Siti Aningsih', 'XI.AGAMA', 'cc3b5fd72dbe8f5d6c6ed2a13c37f274'),
('0005737641', 'Susilo Amin Sungkowo', 'XI.AGAMA', '7fc2808557e23fe56f2fd5148bd81ebd'),
('0020888936', 'Wildani Maulanal Khakim', 'XI.AGAMA', '1ce1092db51299245098acfec8a38fa2'),
('0023970476', 'Wiwit Vika Oktaviana', 'XI.AGAMA', 'ead54eee95e6082cb3eef3b28c1d3dcb'),
('0011744167', 'Yudhistira', 'XI.AGAMA', 'f014d800fec18677a8276c9ce8077ab0');


--
-- Struktur untuk view `viewActions`
--

CREATE VIEW `viewActions`  AS  select `pe_problems`.`nis` AS `nis`,`pe_problems`.`problem_id` AS `problem_id`,`pe_mission`.`mission_id` AS `mission_id`,`pe_actions`.`action_id` AS `action_id`,`pe_siswa`.`namaSiswa` AS `namaSiswa`,`pe_problems`.`problem_type` AS `problem_type`,`pe_problems`.`problem_item` AS `problem_item`,`pe_mission`.`mission_desc` AS `mission_desc`,`pe_mission`.`mission_trgt` AS `mission_trgt`,`pe_actions`.`action_date` AS `action_date`,`pe_actions`.`action_desc` AS `action_desc` from (((`pe_actions` join `pe_mission`) join `pe_problems`) join `pe_siswa`) where ((`pe_mission`.`mission_id` = `pe_actions`.`mission_id`) and (`pe_problems`.`problem_id` = `pe_mission`.`problem_id`) and (`pe_siswa`.`nis` = `pe_problems`.`nis`)) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pe_actions`
--
ALTER TABLE `pe_actions`
  ADD PRIMARY KEY (`action_id`);

--
-- Indeks untuk tabel `pe_evaluation`
--
ALTER TABLE `pe_evaluation`
  ADD PRIMARY KEY (`eval_id`);

--
-- Indeks untuk tabel `pe_group`
--
ALTER TABLE `pe_group`
  ADD PRIMARY KEY (`groupId`);

--
-- Indeks untuk tabel `pe_groupTopics`
--
ALTER TABLE `pe_groupTopics`
  ADD PRIMARY KEY (`gtId`);

--
-- Indeks untuk tabel `pe_mission`
--
ALTER TABLE `pe_mission`
  ADD PRIMARY KEY (`mission_id`);

--
-- Indeks untuk tabel `pe_problems`
--
ALTER TABLE `pe_problems`
  ADD PRIMARY KEY (`problem_id`);

--
-- Indeks untuk tabel `pe_siswa`
--
ALTER TABLE `pe_siswa`
  ADD PRIMARY KEY (`nis`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pe_actions`
--
ALTER TABLE `pe_actions`
  MODIFY `action_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `pe_evaluation`
--
ALTER TABLE `pe_evaluation`
  MODIFY `eval_id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pe_group`
--
ALTER TABLE `pe_group`
  MODIFY `groupId` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pe_groupTopics`
--
ALTER TABLE `pe_groupTopics`
  MODIFY `gtId` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pe_mission`
--
ALTER TABLE `pe_mission`
  MODIFY `mission_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `pe_problems`
--
ALTER TABLE `pe_problems`
  MODIFY `problem_id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
