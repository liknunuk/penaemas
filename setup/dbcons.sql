DROP TABLE IF EXISTS pe_siswa;
CREATE TABLE pe_siswa(
  nis varchar(10) NOT NULL PRIMARY KEY,
  namaSiswa varchar(30) DEFAULT NULL,
  kelas varchar(10) DEFAULT NULL,
  passkey varchar(32) -- md5('nis**password')
);

-- dummy data siswa
INSERT INTO pe_siswa VALUES ('12345','Siswa Uji Coba','XI-A',md5('12345**123456'));

DROP TABLE IF EXISTS  pe_problems;
CREATE TABLE pe_problems(
  problem_id int(4) unsigned auto_increment primary key,
  nis int(5),
  problem_type varchar(11) DEFAULT 'Kepribadian',
  problem_item tinytext,
  logTime timestamp default current_timestamp(),
  status enum ('Berproses','Proses Lanjut','Tersolusikan') default 'Berproses'
);

DROP TABLE IF EXISTS pe_mission;
CREATE TABLE pe_mission(
  mission_id int(5) unsigned auto_increment primary key,
  problem_id int(4) unsigned not null,
  mission_sq int(2) unsigned default 1,
  mission_desc varchar(50) default null,
  mission_trgt varchar(50) default null,
  logTime timestamp default current_timestamp(),
  status enum ('Selesai','Tertunda') default 'Tertunda'
);

DROP TABLE IF EXISTS pe_actions;
CREATE TABLE pe_actions(
  action_id int(6) unsigned auto_increment primary key,
  mission_id int(5) unsigned not null,
  action_date date,
  action_desc tinytext
);

DROP TABLE IF EXISTS pe_evaluation;
CREATE TABLE pe_evaluation(
  eval_id int(8) unsigned auto_increment primary key,
  action_id int(6) unsigned not null,
  logTime timestamp default current_timestamp(),
  personName varchar(30) default null,
  eval_chat tinytext
);

-- GROUPS
DROP TABLE IF EXISTS pe_group;
CREATE TABLE pe_group(
  groupId int(4) not null auto_increment primary key,
  groupMembers tinytext
);

-- GROUP CHAT TOPIC
DROP TABLE IF EXISTS pe_groupTopics;
CREATE TABLE pe_groupTopics(
  gtId int(4) not null auto_increment primary key,
  groupId int(4) not null,
  initiator varchar(10) not null,
  problem_type varchar(11) DEFAULT 'Kepribadian',
  topic tinytext
);

-- GROUP CHAT
DROP TABLE IF EXISTS pe_groupChat;
CREATE TABLE pe_groupChat(
  chatTime timestamp default current_timestamp(),
  groupTopic int(4) not null,
  participant varchar(10),
  shout tinytext
);

-- VIEWS
CREATE OR REPLACE VIEW viewActions AS
SELECT pe_problems.nis,pe_problems.problem_id,pe_mission.mission_id, action_id,namaSiswa,problem_type,problem_item,mission_desc,
mission_trgt,action_date,action_desc
FROM pe_actions, pe_mission, pe_problems,pe_siswa
WHERE pe_mission.mission_id = pe_actions.mission_id &&
pe_problems.problem_id = pe_mission.problem_id
&& pe_siswa.nis = pe_problems.nis;
