<?php
  require("./lib/class.pe.inc.php");
  $pe = new goldenrice();
 ?>
<h4><small>PADI EMAS</small></h4>
<hr>
<h2>Misi Penyelesaian Masalah</h2>
<?php
  $demas = $pe->masalahPapar($_GET['pid']);
?>
<div class="table-responsive">
  <table class="table table-bordered">
    <tr>
      <td width="200">Nama Siswa</td>
      <td><?php echo $demas['namaSiswa'];?></td>
    </tr>
    <tr>
      <td width="200">Nomor Induk</td>
      <td><?php echo $demas['nis'];?></td>
    </tr>
    <tr>
      <td width="200">Kelompok Masalah</td>
      <td><?php echo $demas['problem_type'];?></td>
    </tr>
    <tr>
      <td width="200">Permasalahan</td>
      <td><?php echo $demas['problem_item'];?></td>
    </tr>
  </table>
  <table class="table table-striped">
    <thead>
      <tr>
        <th width="240">Tanggal</th>
        <th>Nama Misi</th>
        <th>Target</th>
        <th width='120'>Status</th>
        <th width='120'>Kontrol</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $misi = $pe->misiAksiGelar($_GET['pid']);
      $i = 0;
      while($i < COUNT($misi)) {
        echo "
        <tr>
          <td>".$pe->timestampConvert($misi[$i]['logTime'])."</td>
          <td>".$misi[$i]['mission_desc']."</td>
          <td>".$misi[$i]['mission_trgt']."</td>
          <td>".$misi[$i]['status']."</td>
          <td>
            <a class='btn btn-default' href='./?data=aksi&mid=".$misi[$i]['mission_id']."'><i class='fa fa-search'></i></a>
            <a class='btn btn-danger msi_stop' id='".$misi[$i]['mission_id']."'><i class='fa fa-check-square-o'></i></a>
          </td>
        </tr>
        ";
        $i++;
      }
       ?>
    </tbody>
  </table>
</div>
