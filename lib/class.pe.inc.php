<?php
  require("class.crud.inc.php");
  class goldenrice extends dbcrud
  {
    function siswaAnyar($sets,$data){
      $this->insert('pe_siswa',$sets,$data);
    }

    function siswaGanti($sets,$data,$key){
      $this->update('pe_siswa',$sets,$data,$key);
    }

    function siswaBuang($nis){
      $this->delete("pe_siswa" , "nis" , $nis );
    }

    function siswaGelar($rowBegin){
      $siswas = $this->select("*","pe_siswa","namaSiswa",$rowBegin);
      return($siswas);
    }

    function siswaLogin($nis,$pwd){
      $passkey=md5($nis."**".$pwd);
      $cred = $this->pickone("*","pe_siswa","passkey",$passkey);
      return $cred;
    }

    function masalahGelar($kelas = 'XI.IPA.1'){
      $sql = "SELECT pe_problems.*, pe_siswa.namaSiswa, pe_siswa.kelas FROM pe_problems, pe_siswa WHERE pe_siswa.nis = pe_problems.nis && pe_problems.status = 'Berproses' && pe_siswa.kelas = ? ORDER BY pe_problems.logTime DESC";
      $qry = $this->transact($sql, array($kelas));
      $problems = array();
      while($r = $qry->fetch()){
        array_push($problems,$r);
      }
      return $problems;
      $qry = null;
    }

    function masalahPapar($pid){
      $mslh = $this->pickone("nis, namaSiswa, problem_type, problem_item","viewActions","problem_id",$pid);
      return($mslh);
    }

    function misiAksiGelar($pid){
      $misi = $this->picksome("*","pe_mission","problem_id='".$pid."' && status='Tertunda'");
      return $misi;
    }

    function aksiPapar($mid){
      $aksi = $this->picksome("*","pe_actions","mission_id='".$mid."'");
      return $aksi;
    }

    function pilihKelas(){
      $sql = "SELECT distinct(kelas) FROM pe_siswa";
      $qry = $this->transact($sql);
      while( $res = $qry->fetch() ){
        echo "<option>".$res['kelas']."</option>";
      }
    }

    function konsepok(){
      $sql = "SELECT pe_groupChat.groupTopic, pe_problems.problem_item , pe_siswa.namaSiswa , pe_problems.status FROM pe_groupChat , pe_problems , pe_siswa
      WHERE pe_problems.problem_id = pe_groupChat.groupTopic && pe_siswa.nis = pe_problems.nis && pe_problems.status ='Berproses'
      GROUP BY pe_groupChat.groupTopic
      ORDER BY pe_groupChat.groupTopic";

      $qry = $this->transact($sql);
      $diskusi = [];
      while($res = $qry->fetch()){
        array_push($diskusi,$res);
      }

      return $diskusi;
      $qry->closeCursor();
    }

    function gchat($pid){
      $sql = "SELECT pe_groupChat.chatTime , pe_siswa.namaSiswa , pe_groupChat.shout FROM `pe_groupChat` , pe_siswa WHERE `groupTopic`= {$pid} && pe_siswa.nis = pe_groupChat.participant ORDER BY chatTime LIMIT 60";

      $qry = $this->transact($sql);
      $chats = [];
      while($res = $qry->fetch()){
        echo "
        <li class='list-group-item'><small>{$res['namaSiswa']} pada {$res['chatTime']} :</small> {$res['shout']}
        </li>
        ";
      }
    }

    function pokseling($pn = 1){
      $row = ($pn - 1) * 60;
      $sql = "SELECT * FROM pe_group ORDER BY groupId LIMIT $row , 60";
      $qry = $this->transact($sql);
      $klp = [];
      while( $res = $qry->fetch()){
        $iswa = [];
        $siswa = explode(',',$res['groupMembers']);
        // print_r($siswa) . "<br/>";
        
        for($i = 0 ; $i < 10 ; $i++ ){
          // echo $siswa[$i] . "<br/>"; 
          $indx = $siswa[$i];
          $data = $this->nis2siswa($indx);
          array_push($iswa,$data);          
        }
        array_push($klp, array('gid'=> $res['groupId'] , 'siswa'=>$iswa));
      }
      return($klp);
      $qry->closeCursor();
    }

    function nis2siswa($nis){
      $sql = "SELECT nis, namaSiswa, kelas FROM pe_siswa WHERE nis = '{$nis}' LIMIT 1";
      $qry = $this->transact($sql);
      $res = $qry->fetch();
      return $res;
      $qry->closeCursor();
    }


  }
/* 
group chat and group id
SELECT a.* , b.groupId FROM pe_groupChat a , pe_group b WHERE b.groupMembers LIKE CONCAT('%' , a.participant , '%') */
?>