<div class="row">
    <div class="col-md-12">
    <h3>DISKUSI KONSELING KELOMPOK</h3>
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th>Id Diskusi</th>
                    <th>Tema</th>
                    <th>Inisiator</th>
                    <th>Status</th>
                </tr>
            </thead>
            <?php
                require("./lib/class.pe.inc.php");
                $pe = new goldenrice();
                $konkel = $pe->konsepok();
            ?>
            <tbody>
                <?php
                    for($i = 0 ; $i < COUNT($konkel) ; $i++ ){
                        echo "
                        <tr>
                            <td>{$konkel[$i]['groupTopic']}</td>
                            <td>
                                <a href='./?data=gconc&pid={$konkel[$i]['groupTopic']}'>
                                {$konkel[$i]['problem_item']}
                                </a>
                            </td>
                            <td>{$konkel[$i]['namaSiswa']}</td>
                            <td>{$konkel[$i]['status']}</td>
                        </tr>
                        ";
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="list-group" id="gchat">
        <?php
        if(isset($_GET['pid'])){
            $chat = $pe->gchat($_GET['pid']);
        }
        $pid = $_GET['pid']; 
        unset($_GET['pid'])
        ?>
        </div>
        <div>
            <form action="gshout.php" method="post">
            <input type="hidden" name="pid" value=<?= $pid;?> >
                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <input type="text" name="kataguru" id="kataguru" class='form-control'>
                                </div>
                            </td>
                            <td width="80px">
                                <input type="submit" value="Kirim" class="btn btn-primary">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
