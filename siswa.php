<h4><small>PADI EMAS</small></h4>
<hr>
<h2>Daftar Siswa &nbsp;
  <span>
  <a href='./?data=fsiswa&mode=baru'>
    <i class="fa fa-plus-square" aria-hidden="true"></i>
  </a>
</span></h2>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th width="100">Nomor Induk</th>
        <th>Nama Siswa</th>
        <th>Kelas</th>
        <th width='150'>Kontrol</th>
      </tr>
    </thead>
    <tbody>
      <?php
      require_once("./lib/class.pe.inc.php");
      $pe = new goldenrice();
      if(!$_GET['nh']){ $nh = 0; }else{ $nh = ($_GET['nh'] -1 ) * rows; }
      $dsiswa = $pe->siswaGelar($nh);
      $i = 0 ;
      while($i < COUNT($dsiswa)){
        echo "
        <tr>
          <td>".$dsiswa[$i]['nis']."</td>
          <td>".$dsiswa[$i]['namaSiswa']."</td>
          <td>".$dsiswa[$i]['kelas']."</td>
          <td>
            <a href='./?data=fsiswa&mode=ubah&nis=".$dsiswa[$i]['nis']."'>
              <i class='fa fa-pencil-square-o fa-2x'></i>
            </a>
            <a href='hapus-siswa.php?nis=".$dsiswa[$i]['nis']."'>
              <i class='fa fa-remove fa-2x'></i>
            </a>
          </td>
        </tr>
        ";
        $i++;
      }
      ?>

    </tbody>
  </table>
  <div>
      <table width="100%">
      <?php
      $pn = !$_GET['nh'] ? 1 : $_GET['nh'];
      ?>
        <tr>
          <td class="text-center">
            <a href="javascript:void(0)" onClick=halseb("siswa",<?=$pn;?>)>Halaman Sebelumnya</a>
            <input type="number" id="pn" style = "width:75px; padding: 1px 5px; margin: 0px 20px;" value="<?=$pn;?>" min=1>
            <a href="javascript:void(0)" onClick=halber("siswa",<?=$pn;?>)>Halaman Berikutnya</a>
          </td>
        </tr>
      </table>
  </div>
</div>
