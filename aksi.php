<?php
  require("./lib/class.pe.inc.php");
  $pe = new goldenrice();
 ?>
<h4><small>PADI EMAS</small></h4>
<hr>
<h2>Aksi Penyelesaian Masalah</h2>
<?php
  $demis = $pe->pickone("*","viewActions","mission_id",$_GET['mid']);
?>
<div class="table-responsive">
  <table class="table table-bordered">
    <tr>
      <td width="200">Nama Siswa</td>
      <td><?php echo $demis['namaSiswa'];?></td>
    </tr>
    <tr>
      <td width="200">Nomor Induk</td>
      <td><?php echo $demis['nis'];?></td>
    </tr>
    <tr>
      <td width="200">Kelompok Masalah</td>
      <td><?php echo $demis['problem_type'];?></td>
    </tr>
    <tr>
      <td width="200">Permasalahan</td>
      <td><?php echo $demis['problem_item'];?></td>
    </tr>
    <tr>
      <td width="200">Misi Penyelesaian</td>
      <td><?php echo $demis['mission_desc'] .", ".$demis['mission_trgt'];?></td>
    </tr>
  </table>
  <div>
    <h3>Aksi Penuntasan Misi</h3>
  </div>
  <table class="table table-striped">
    <tbody>
      <?php
      $aksi = $pe->aksiPapar($_GET['mid']);
      $i = 0;
      while($i < COUNT($aksi)) {
        echo "
        <tr>
          <td>
            ".$aksi[$i]['action_desc']." (".$pe->tanggalTerbaca($aksi[$i]['action_date']).")
            <a href='javascript:void(0)' onClick=shoutReply('".$aksi[$i]['action_id']."')>
              &nbsp;<i class='fa fa-reply'></i> Evaluasi
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <table class='table table-striped'>
              <thead>
                <tr>
                  <th>Jam / Tanggal</th>
                  <th>Pembicara</th>
                  <th>Pembicaraan</th>
                </tr>
              </thead>
              <tbody>";
              $eval=$pe->picksome("*","pe_evaluation","action_id='".$aksi[$i]['action_id']."'");
              for($j = 0 ; $j < COUNT($eval) ; $j++){
                echo "
                <tr>
                  <td>".$pe->timestampConvert($eval[$j]['logTime'])."</td>
                  <td>".$eval[$j]['personName']."</td>
                  <td>".$eval[$j]['eval_chat']."</td>
                </tr>
                ";
              }
        echo "
              </tbody>
            </table>
          </td>
        </tr>
        ";
        $i++;
      }
       ?>
    </tbody>
  </table>
</div>
<!-- modals -->
<!-- Modal -->
<div id="shoutModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Evaluasi Aksi</h4>
      </div>
      <div class="modal-body">
        <form action="shout.php" method="post">
        <div class='form-group'>
          <input type='text' id='aid' name="aid" class="form-control" readonly />
        </div>
        <div class='form-group'>
          <input type='text' id='shout' name="shout" class="form-control" />
        </div>
        <div class='form-group'>
          <input type='submit' id='shout-submit' class="form-control btn btn-success" value="Evaluasi" />
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>

  function shoutReply(aid){
    $("#shoutModal").modal('show');
    $("#aid").val(aid);
  }

</script>
