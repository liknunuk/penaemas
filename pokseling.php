<div class="row">
    <div class="col-md-12">
    <h3>
        KELOMPOK KONSELING
        <a href='./?data=fgconc&mode=baru'>
            <i class="fa fa-plus-square" aria-hidden="true"></i>
        </a>
    </h3>
    <?php
        require("./lib/class.pe.inc.php");
        $pe = new goldenrice();
        $poksel = $pe->pokseling(1);
    ?>
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th>No. Kelompok</th>
                    <th>Siswa</th>
                    <th>Kontrol</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for($i = 0 ; $i < COUNT($poksel) ; $i++ ){
                        echo "
                        <tr>
                            <td>Kelompok {$poksel[$i]['gid']}</td>
                            <td>
                               
                            ";
                                for($s = 0 ; $s < 10 ; $s++){
                                    echo " [ " .
                                        $poksel[$i]['siswa'][$s]['namaSiswa']." - ". $poksel[$i]['siswa'][$s]['kelas'] . " ] ,";
                                }

                        echo "    
                                
                            </td>
                            <td>
                                <a href='./?data=fgconc&mode=edit&gid=".$poksel[$i]['gid']."'>
                                Edit</a></td>
                            <td><a href='javascript:void(0)' class='rgconc' id='{$poksel[$i]['gid']}'>Hapus</a></td>
                        </tr>
                        ";
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
