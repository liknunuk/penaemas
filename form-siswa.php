<?php
  require_once("./lib/class.pe.inc.php");
  $pe = new goldenrice();
  if($_GET['mode'] == "baru" ){
    $nis    = "";
    $nama   = "";
    $kelas  = "";
  }else{
    $ds = $pe->pickone("*","pe_siswa","nis",$_GET['nis']);
    $nis    = $ds['nis'];
    $nama   = $ds['namaSiswa'];
    $kelas  = $ds['kelas'];
  }
?>

<div>
  <h2>Formulir Penambahan Siswa Padi Emas</h2><br />
</div>
<form action="act-siswa.php?mod=<?php echo $_GET['mode']; ?>" method="post" class="form-horizontal"/>
  <div class="form-group">
    <label class="col-sm-3">Nomor Induk</label>
    <div class='col-sm-9'>
      <input class="form-control" name="nis" type="number" maxlength="5" value="<?php echo $nis; ?>" />
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3">Nama Siswa</label>
    <div class='col-sm-9'>
      <input class="form-control" name="namaSiswa" type="text" maxlength="30" value="<?php echo $nama; ?>" />
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3">Kelas</label>
    <div class='col-sm-9'>
      <input class="form-control" name="kelas" type="text" maxlength="6" value="<?php echo $kelas; ?>" />
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3">Kata Sandi</label>
    <div class='col-sm-9'>
      <input class="form-control" name="passkey" type="password" maxlength="16"
      value="123456" />
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 bg-danger">Cek Data ..!</label>
    <div class='col-sm-9' style='text-align:right; padding-right: 20px;'>
      <input type="submit" class="btn btn-primary" value="SimpaN" />
    </div>
  </div>
</form>
