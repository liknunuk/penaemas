<?php
require("./lib/class.pe.inc.php");
$pe = new goldenrice();

if($_POST['obj'] == 'problem'){
    $sql = "UPDATE pe_problems SET status = ? WHERE problem_id = ? ";
    $qry = $pe->transact($sql,array($_POST['stt'],$_POST['oid']));
    $qry->closeCursor();
}

if($_POST['obj'] == 'mission'){
    $sql = "UPDATE pe_mission SET status = 'Selesai' WHERE mission_id = ? ";
    $qry = $pe->transact($sql,array($_POST['oid']));
    $qry->closeCursor();
}
?>