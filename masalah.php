<?php
  require("./lib/class.pe.inc.php");
  $pe = new goldenrice();
 ?>
<h4><small>PADI EMAS</small></h4>
 <div class="row">
  <div class="col-sm-6">
    <?php 
    if(!$_GET['kelas']){
      $kelas = 'XI.IPA.1';
    }else{
      $kelas = $_GET['kelas'];
    }
    ?>
    <h2>Daftar Masalah Siswa <?=$kelas;?> </h2>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="kelas">Pilih kelas</label>
      <select class="form-control" id="prb_kelas">
      <option>Pilih Kelas</option>
      <?php
      $pe->pilihKelas();
      ?>
      </select>
    </div>
  </div>
</div>
<hr>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th width="120">Nomor Induk</th>
        <th>Nama Siswa</th>
        <th>Jenis Masalah</th>
        <th>Masalah</th>
        <th>Status</th>
        <th width='150'>Kontrol</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!$_GET['kelas']){
        $masalah = $pe->masalahGelar();
      }else{
        $masalah = $pe->masalahGelar($_GET['kelas']);
      }
      $i = 0;
      while($i < COUNT($masalah)) {
        echo "
        <tr>
          <td>".$masalah[$i]['nis']."</td>
          <td>".$masalah[$i]['namaSiswa']."<br/>[".$masalah[$i]['logTime']."]</td>
          <td>".$masalah[$i]['problem_type']."</td>
          <td>".$masalah[$i]['problem_item']."</td>
          <td>".$masalah[$i]['status']."</td>
          <td>
            <a class='btn btn-default' href='./?data=misi&pid=".$masalah[$i]['problem_id']."'><i class='fa fa-search'></i></a>
            <a class='btn btn-warning prb_cont' id='".$masalah[$i]['problem_id']."'> <i class='fa fa-check-square-o'></i></a>
            <a class='btn btn-danger prb_stop' id='".$masalah[$i]['problem_id']."'> <i class='fa fa-check-square-o'></i></a>
          </td>
        </tr>
        ";
        $i++;
      }
       ?>
    </tbody>
  </table>
</div>
